﻿#include "TestGame.h"
#include "../maths/maths.h"
#include "../graphics/renderables/Renderable3D.h"
#include "../graphics/renderables/Renderable2D.h"
#include "../graphics/objects/Camera.h"
#include <vector>
#include <random>
#include <time.h>
#include "../graphics/objects/ModelManager.h"
#include "../graphics/textures/TextureManager.h"
#include "../graphics/objects/MeshTools.h"
#include "../noise/NoiseFunction.h"
#include "../noise/PerlinNoise.h"
#include <SOLOUD/soloud.h>
#include <SOLOUD/soloud_wav.h>
#include "../graphics/fonts/Font.h"
#include "../graphics/fonts/FontManager.h"
#include "../graphics/renderables/RenderableText.h"
#include <thread>
#include <list>
#include <ctime>

// All models used in the scene
std::vector<Renderable3D> models;

// Camera and view information
//Camera camera = Camera(Vec3(0, 35, 0), Vec3(0, 35, 0) + Vec3(0, -1, -1));
FpsCam camera = FpsCam(Vec3(0));
float hRotation = 0;
float vRotation = 0;
float sensitivity = 0.1;
float movespeed = 0.6f;

// Perspective matrix
Mat4 pers;

// Orthogonal matrix
Mat4 ortho;

// Boolean to determine whether to use the perspective matrix (true) or orthogonal matrix (false)
bool usePers;

// All lights for the scene
DirectionalLight sunLight = DirectionalLight(Vec3(0, -1, -1), Vec3(1));
std::vector<PointLight> lights;

// Counter to prevent user from creating lights too quickly
int lightTimer = 0;

// Audio objects and handlers
SoLoud::Soloud soloud;
SoLoud::Wav wav;
SoLoud::handle handle;

// Position used for determine audio volume and direction
Vec3 soundPos;

// On screen text objects
Font* font;
RenderableText* tpsDisplay;
RenderableText* fpsDisplay;
RenderableText* posDisplay;

// Noise function used for terrain
PerlinNoise2D perlin(1);





using namespace Noise;

uint16_t RandomU16(uint16_t gRandomSeed16);

void TestGame::Init() {

	window->setWindowMode(Window::WINDOWED);
	soloud.init();
	usePers = true;
	
	int above = 0;
	int below = 1;
	
	uint16_t ran = RandomU16(0);
	while (ran) {
		if (ran >= 0x7FFF) above++;
		else below++;
		ran = RandomU16(ran);
	}
	std::cout << "Above " << above << " : Below " << below << std::endl;
	if (RandomU16(22026) == RandomU16(0))
		std::cout << "SAME"  << std::endl;
}

uint16_t RandomU16(uint16_t gRandomSeed16) {
	uint16_t temp1, temp2;

	if (gRandomSeed16 == 22026) {
		gRandomSeed16 = 0;
	}

	temp1 = (gRandomSeed16 & 0x00FF) << 8;
	temp1 = temp1 ^ gRandomSeed16;

	gRandomSeed16 = ((temp1 & 0x00FF) << 8) + ((temp1 & 0xFF00) >> 8);

	temp1 = ((temp1 & 0x00FF) << 1) ^ gRandomSeed16;
	temp2 = (temp1 >> 1) ^ 0xFF80;

	if ((temp1 & 1) == 0) {
		if (temp2 == 43605) {
			gRandomSeed16 = 0;
		}
		else {
			gRandomSeed16 = temp2 ^ 0x1FF4;
		}
	}
	else {
		gRandomSeed16 = temp2 ^ 0x8180;
	}

	return gRandomSeed16;
}

void TestGame::Resize(int width, int height) {
	// Re-calculates the projection matrices to fit the new aspect ratio
	float aspr = (float)width / float(height);
	pers = Mat4::persp(60, aspr, 1, 100000);
	ortho = Mat4::ortho(10, -10, -10 * aspr, 10 * aspr, 0, 1000);



}

void TestGame::Load() {

	sunLight.ambient = Vec3(0.2);
	sunLight.specular = Vec3(0);

	font = FontManager::loadBitmapFont("fonts/ComicNeue.fnt");

	float displaySize = 40;

	fpsDisplay = new RenderableText("FPS: ", font);
	fpsDisplay->position = Vec2(0, 0);
	fpsDisplay->size = displaySize;

	tpsDisplay = new RenderableText("TPS: ", font);
	tpsDisplay->position = Vec2(0, displaySize);
	tpsDisplay->size = displaySize;

	posDisplay = new RenderableText("POS: ", font);
	posDisplay->position = Vec2(0, displaySize*2);
	posDisplay->size = displaySize;


	// https://snappygoat.com/free-public-domain-images-earth_nasa_map_day/DgNts4J8Tbhd9WqdD7DpGK4SjK-Iq-4wmigcSiRw538.html
	Texture* globe = TextureManager::getTexture("textures/globe.png");

	// https://blog.playcanvas.com/wp-content/uploads/2016/10/Earth-Color4096.jpg
	Texture* globeSD = TextureManager::getTexture("textures/Earth-Color4096.jpg");

	// http://www.shadedrelief.com/natural3/pages/textures.html
	Texture* globeHD = TextureManager::getTexture("textures/1_earth_8k.jpg");

	Texture* rainbow = TextureManager::getTexture("textures/mountain.png");

	//Texture* grid = TextureManager::getTexture("textures/256Grid.png");

	Vec2 pieceSize = Vec2(80, 80);


	//models.push_back(Renderable3D(Vec3(-25, 100, 0), ModelManager::getModel(MeshTools::Box(Vec3(30), texture))));

	//models.push_back(Renderable3D(Vec3(50, -6731000, 0), ModelManager::getModel(MeshTools::UVSphere(512, 1024, globeHD))));
	//models.back().scale = Vec3(6731000);

	

	//models.push_back(Renderable3D(Vec3(25, 100, 0), ModelManager::getModel(MeshTools::Box(Vec3(30), texture))));

	models.push_back(Renderable3D(Vec3(0), ModelManager::getModel("textures/WF.obj")));
	
	
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	inputHandler->disableCursor();


	wav.load("sounds/Hypnotic-Puzzle3.mp3");
	soloud.setVolume(handle, 0.4);
	//soloud.play(wav);
}


void TestGame::Draw() {
	renderer->setCamera(&camera);
	
	if(usePers)renderer->setProjectionMatrix(&pers);
	else renderer->setProjectionMatrix(&ortho);

	// Send all models to the renderer
	for (unsigned int i = 0; i < models.size(); i++) {
		Renderable3D* renderable = &models[i];
		renderer->draw(renderable);
	}

	// Sends all poinr lights to the renderer
	for (unsigned int i = 0; i < lights.size(); i++) {
		renderer->addLight(&lights[i]);
	}

	// Sends sunlight to the renderer
	renderer->addLight(&sunLight);

	// Sends the screen text to the renderer
	renderer->draw(tpsDisplay);
	renderer->draw(fpsDisplay);
	renderer->draw(posDisplay);
	
}

void TestGame::Update() {

	time_t t = std::time(nullptr);
	tm* time = std::gmtime(&t);
	int seconds = 0;
	seconds += time->tm_sec;
	seconds += 60 * time->tm_min;
	seconds += 60 * 60 * time->tm_hour;
	float earthRotation = (float)seconds / (24 * 60 * 60);
	models.back().yaw = earthRotation * pi * 2;
	std::cout << earthRotation << std::endl;


	// Updates the values in the text objects
	tpsDisplay->text = "TPS: " + to_string(tps);
	fpsDisplay->text = "FPS: " + to_string(fps);
	posDisplay->text = "POS: " + camera.position.toString();
	
	
	if (lightTimer > 0) lightTimer--;

	float camspeed = 0.8f;
	
	if (inputHandler->isDown('=')) movespeed *= 1.1;
	if (inputHandler->isDown('-')) movespeed /= 1.1;

	camspeed = movespeed;
	if (inputHandler->isDown('D'))
	{
		
		camera.position.add(camera.getRight() * movespeed);
	}
	else if (inputHandler->isDown('A'))
	{
		camera.position.add(camera.getLeft() * movespeed);
	}

	if (inputHandler->isDown('W'))
	{
		camera.position.add(camera.getDirection() * movespeed);
	}
	else if (inputHandler->isDown('S'))
	{
		camera.position.add(camera.getBack() * movespeed);
	}

	// Move camera up/down with space/shift
	if (inputHandler->isDown(GLFW_KEY_SPACE)) camera.position += camera.getUp() * camspeed;
	if (inputHandler->isDown(GLFW_KEY_LEFT_SHIFT)) camera.position += camera.getDown() *  camspeed;


	// Old keyboard camera controls using arrow keys
	if (inputHandler->isDown(GLFW_KEY_UP)) vRotation++;
	if (inputHandler->isDown(GLFW_KEY_DOWN)) vRotation--;

	if (inputHandler->isDown(GLFW_KEY_LEFT)) hRotation--;
	if (inputHandler->isDown(GLFW_KEY_RIGHT)) hRotation++;


	if (inputHandler->isDown(GLFW_KEY_ESCAPE)) window->close();

	if (inputHandler->isDown('R') && lightTimer == 0) {
		lightTimer = 10;
		
		Vec3 color;
		color.x = (rand() % 10000) / 100.0f;
		color.y = (rand() % 10000) / 100.0f;
		color.z = (rand() % 10000) / 100.0f;
		color.normalise();
		color = color;

		PointLight light(camera.position, color);
		float brightness = 200;
		light.constant /= brightness;
		light.linear /= brightness;
		light.quadratic /= brightness;

		lights.push_back(light);

	}
	if (inputHandler->isDown('L'))
	{
		sunLight.direction = camera.getDirection();
	}

	if (inputHandler->isDown('P')) usePers = true;
	else if (inputHandler->isDown('O')) usePers = false;

	hRotation += inputHandler->xpos * sensitivity;
	vRotation -= inputHandler->ypos * sensitivity;

	if (vRotation < -90.0f) vRotation = -90.0f;
	if (vRotation > 90.0f) vRotation = 90.0f;

	camera.setRotation(hRotation, vRotation);
	//camera.addYaw(hRotation);
	//camera.addPitch(vRotation);
	
	soloud.set3dListenerPosition(camera.position.x, camera.position.y, camera.position.z);
	Vec3 target = camera.getDirection();
	soloud.set3dListenerAt(target.x, target.y, target.z);
	soloud.update3dAudio();


	//box->position = Vec3(0, 200, 0);
	//Vec3 axis = camera.calcTarget(hRotation, vRotation);
	//Vec3 vec = camera.calcTarget(hRotation, vRotation + 90);

	//box->position = Vec3::rotate(vec * 30, axis, glfwGetTime() * 2) + camera.position + axis * 150;
	
}

void TestGame::Exit() {
	soloud.deinit();
}
