#pragma once

#include "../core/Game.h"



class TestGame : public Game {

	void Init() override;
	void Load() override;
	void Draw() override;
	void Update() override;
	void Exit() override;
	void Resize(int width, int height) override;



};
