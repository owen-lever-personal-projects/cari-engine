#pragma once

#include "../input/InputHandler.h"

namespace CariEngine {
	using namespace Input;
	class Window {



	

	public:
		enum WindowMode {
			FULLSCREEN,
			BORDERLESS,
			WINDOWED
		};

		Window(const char* name, int width, int height, int flags = 0);
		~Window();

		void init();

		void update();
		bool closed();
		void close();
		void clear();
		void setWindowMode(WindowMode mode);


		int width;
		int height;
		WindowMode windowMode;
		GLFWwindow* window;
		//Game* game;
	protected:
		void resizeEvent(int width, int height);
			
	private:
		GLFWmonitor* monitor;
		
		const char* title;
		bool initialised;
		




	};

}