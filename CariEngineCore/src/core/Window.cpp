#include "Window.h"
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>

namespace CariEngine {

	
	

	Window::Window(const char* title, int width, int height, int flags) {
		initialised = false;
		windowMode = WINDOWED;

		this->title = title;
		this->height = height;
		this->width = width;

		

		if (!glfwInit() ) {
			std::cout << "GLFW failed to initialise" << std::endl;
		}


		// Sets depth buffer precision to 32 bits
		glfwWindowHint(GLFW_DEPTH_BITS, 32);

		// Enables multisampling with 4 samples
		glfwWindowHint(GLFW_SAMPLES, 4);

		// Sets the window to be invisible on creation
		// This is so the window can be made visible during the Window::init() method, allowing devs to change the window size/title before it's shown
		glfwWindowHint(GLFW_VISIBLE, GL_FALSE);


		monitor = glfwGetPrimaryMonitor();
		window = glfwCreateWindow(width, height, title, NULL, NULL);

		

		if (!window) {
			std::cout << "Failed to create GLFW window" << std::endl;
		}
		
		glfwMakeContextCurrent(window);


		glEnable(GL_DEBUG_OUTPUT);

		if (int err = glewInit() != GLEW_OK) {
			std::cout << "Glew Init Error: " << err << std::endl;
		}
		
	}

	void Window::init() {
		initialised = true;
		
		if (windowMode != WINDOWED) {
			setWindowMode(windowMode);
		}
		glfwShowWindow(window);

	}

	void Window::setWindowMode(WindowMode mode) {

		this->windowMode = mode;
		
		if (initialised) {
			const GLFWvidmode* vidMode = glfwGetVideoMode(monitor);
			if (mode == WindowMode::BORDERLESS) {


				glfwSetWindowMonitor(window, NULL, 0, 0, vidMode->width, vidMode->height, vidMode->refreshRate);
				glfwSetWindowAttrib(window, GLFW_DECORATED, GL_FALSE);
			}
			else if (mode == WindowMode::FULLSCREEN) {
				glfwSetWindowMonitor(window, monitor, 0, 0, vidMode->width, vidMode->height, vidMode->refreshRate);
			}
			else if (mode == WindowMode::WINDOWED) {
				glfwSetWindowMonitor(window, NULL, 0, 0, vidMode->width, vidMode->height, vidMode->refreshRate);
				glfwSetWindowAttrib(window, GLFW_DECORATED, GL_TRUE);
			}
			
			glfwSetWindowSize(window, vidMode->width, vidMode->height);
			
		}
	}

	Window::~Window() {
		glfwTerminate();
	}

	void Window::update() {

		// Prints out all opengl errors
		GLenum error = glGetError();
		while (error != GL_NO_ERROR) {
			std::cout << "OpenGL error: " << error << std::endl;
			error = glGetError();
		}

		// Updates the window
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	void Window::resizeEvent(int width, int height) {
		this->width = width;
		this->height = height;
	}

	bool Window::closed() {
		return glfwWindowShouldClose(window);
	}

	void Window::close() {
		glfwSetWindowShouldClose(window, GL_TRUE);
	}

	void Window::clear() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

}