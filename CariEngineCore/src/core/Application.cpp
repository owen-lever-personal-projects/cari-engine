#include "Application.h"
#include <string>
#include "../graphics/objects/ModelManager.h"
#include <iostream>
#include <thread>

namespace CariEngine {

	Application* Application::thisApplication;
	InputHandler* Application::inputHandler;

	Application::Application(Game* game) {
		this->game = game;
		thisApplication = this;
	}

	
	
	void Application::init(int flags) {

		// Creates the Window object
		window = new Window("CariEngine", 800, 450);

		// Creates the renderer object
		renderer = new Renderer();

		// Add the renderer and window object to the game
		game->window = window;
		game->renderer = renderer;

		// Initialises the model manager
		ModelManager::init(&renderer->standardShader);
		
		// Sets up the input handler and adds it to the game
		inputHandler = new InputHandler();
		game->inputHandler = inputHandler;
		inputHandler->setWindow(window->window);
		inputHandler->setScreenSize(window->width, window->height);

		// Initialises the game
		game->Init();

		// Initialised the window
		glfwSetWindowSizeCallback(window->window, window_size_callback);
		glfwSetKeyCallback(window->window, key_callback);
		glfwSetCursorPosCallback(window->window, curser_pos_callback);
		window->init();

	}

	void Application::run() {
		game->Load();
		int tps = 60;

		// Sets the time per tick to 1/60th of a second
		double tpt = 1.0 / (double)tps;

		// Sets the lastTick value used for timing to the amount of ticks elapsed since the engine was initialised
		int lastTick = (int)(glfwGetTime() / tpt);
		
		int second = 0;
		int frames = 0;
		int updates = 0;
		
		while (!window->closed()) {
			double time = glfwGetTime();
			int tick = time / tpt;

			// Updates the game object if the current tick value is greater than lastTick
			if (tick > lastTick) {
				update();
				updates++;
				lastTick++;
			}
			// Draws the game if it does not need to be updated
			else {
				frames++;
				draw();
			}
			// Updates the game's tps and fps counter every second
			if ((int)time > second) {
				second = (int)time;
				game->tps = updates;
				game->fps = frames;
				updates = 0;
				frames = 0;
			}
		}
		game->Exit();

	}

	void Application::update() {
		inputHandler->update();
		game->Update();
	}

	void Application::draw() {

		renderer->begin();
		game->Draw();
		renderer->end();

		window->clear();
		renderer->flush();
		window->update();
	}

	void Application::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
		inputHandler->keyCallback(window, key, scancode, action, mods);
	}

	void Application::curser_pos_callback(GLFWwindow* window, double xpos, double ypos) {
		inputHandler->cursorPosCallback(window, xpos, ypos);
	}

	void Application::windowResizeEvent(int width, int height) {
		window->width = width;
		window->height = height;

		game->Resize(width, height);
		
		renderer->resize(width, height);
	}

	void Application::window_size_callback(GLFWwindow* window, int width, int height) {
		glfwGetFramebufferSize(window, &width, &height);
		glViewport(0, 0, width, height);
		thisApplication->windowResizeEvent(width, height);
	}




}