#pragma once

#include "../graphics/renderers/BatchRenderer2D.h"
#include "Window.h"
#include "Game.h"
#include "../graphics/renderers/SkyboxRenderer.h"
#include "../graphics/renderers/Renderer.h"
#include "../input/InputHandler.h"
namespace CariEngine{
	using namespace Graphics;
	
	class Application {


	enum ApplicationFlags {
		CONSOLE = 1 << 0,
	};


	public:
		Application(Game* game);
		void run();
		void init(int flags = 0);
	protected:
		static InputHandler* inputHandler;
	private:

		void windowResizeEvent(int width, int height);

		Renderer* renderer;
		

		Game* game;
		Window* window;
		void update();
		void draw();
		static Application* thisApplication;

		static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void curser_pos_callback(GLFWwindow* window, double xpos, double ypos);
		static void window_size_callback(GLFWwindow* window, int width, int height);
	};





}