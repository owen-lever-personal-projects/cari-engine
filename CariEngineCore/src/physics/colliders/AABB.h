#pragma once
#include "../../maths/maths.h"

namespace CariEngine { namespace Physics{
	using namespace Maths;

	class AABB {
		Vec3 min;
		Vec3 max;

		AABB();
		AABB(const Vec3& min, const Vec3& max);
		bool testCollision(const AABB& other);
		Vec3 getSize();




	};





}}