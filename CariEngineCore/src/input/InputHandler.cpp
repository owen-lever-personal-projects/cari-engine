#include "InputHandler.h"
#include <vector>

namespace CariEngine { namespace Input {
	


	/*
		-1 Key is not held down
		0 key was pressed and released same tick
		1 key was pressed
		1+ nth tick this key was held down for   
	
	*/
	float lastX;
	float lastY;

	bool InputHandler::isPressed(int key) {
		
		if(keys.find(key) != keys.end()) {
			int value = keys[key];
			return (value == 0 || value == 1);
		}
		else {
			keys[key] = -1;
		}
		return false;
	}
	
	bool InputHandler::isDown(int key) {
		if (keys.find(key) != keys.end()) {
			int value = keys[key];
			return (value != -1);
		}
		else {
			keys[key] = -1;
		}
		return false;
	}
	
	int InputHandler::heldFor(int key) {
		if (keys.find(key) != keys.end()) {
			int value = keys[key];
			if (value == 0)
				value = 1;
			return value;
		}
		else {
			keys[key] = -1;
		}
		return -1;
	}

	void InputHandler::update() {
		double nx;
		double ny;
		glfwGetCursorPos(window, &nx, &ny);
		xpos = nx - lastX;
		ypos = ny - lastY;
		lastX = nx;
		lastY = ny;


		std::map<int, int>::iterator it;
		for (it = keys.begin(); it != keys.end(); it++) {
			int k = it->first;
			int v = it->second;
			if (v > 0) {
				keys[k] = v + 1;
			}
			else if (v==0){
				keys[k] = -1;
			}
		}
	}

	void InputHandler::cursorPosCallback(GLFWwindow* window, double xpos, double ypos) {
		//this->xpos = (float)xpos;
		//this->ypos = (float)ypos;
	}

	void InputHandler::enableCursor() {
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}

	void InputHandler::disableCursor() {
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		if (glfwRawMouseMotionSupported()) {
			glfwSetInputMode(window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
		}
	}

	void InputHandler::hideCursor() {
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	}

	void InputHandler::setWindow(GLFWwindow* window) {
		this->window = window;
	}

	void InputHandler::setScreenSize(int width, int height) {
		this->screenWidth = width;
		this->screenHeight = height;
	}
	void InputHandler::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
		if (action == GLFW_PRESS) {
			if (keys.find(key) != keys.end()) {
				if (keys[key] == -1)
					keys[key] = 1;
			}
			else {
				keys[key] = 1;
			}
		}
		else if (action == GLFW_RELEASE) {
			if (keys.find(key) != keys.end()) {
				if (keys[key] == 1) {
					keys[key] = 0;
				}
				else {
					keys[key] = -1;
				}
			}
			else {
				keys[key] = -1;
			}
		}
	}





}}