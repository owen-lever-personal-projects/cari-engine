#include "PerlinNoise.h"
#include <string>

namespace CariEngine { namespace Noise {

	static Vec3 gradientVectors[12] = {
		Vec3(1, 1, 0).normalise(),
		Vec3(1, -1, 0).normalise(),
		Vec3(-1, 1, 0).normalise(),
		Vec3(-1, -1, 0).normalise(),

		Vec3(1, 0, 1).normalise(),
		Vec3(1, 0, -1).normalise(),
		Vec3(-1, 0, 1).normalise(),
		Vec3(-1, 0, -1).normalise(),

		Vec3(0, 1, 1).normalise(),
		Vec3(0, 1, -1).normalise(),
		Vec3(0, -1, 1).normalise(),
		Vec3(0, -1, -1).normalise()

	};

	PerlinNoise3D::PerlinNoise3D(unsigned long seed) {
		this->seed = seed;
		scale = Vec3(1, 1, 1);
	}
	float PerlinNoise3D::noise(float x, float y, float z) {
		x += offset.x;
		y += offset.y;
		z += offset.z;

		x *= scale.x;
		y *= scale.y;
		z *= scale.z;

		int x0 = (int)x;
		int y0 = (int)y;
		int z0 = (int)z;

		x -= x0;
		y -= y0;
		z -= z0;
		
		float tfl = Vec3::dot(Vec3(x	, y-1	, z-1),		getGradient(x0		, y0 + 1	, z0 + 1));
		float tfr = Vec3::dot(Vec3(x - 1, y-1	, z-1),		getGradient(x0 + 1	, y0 + 1	, z0 + 1));

		float tbl = Vec3::dot(Vec3(x	, y-1	, z),		getGradient(x0		, y0 + 1	, z0));
		float tbr = Vec3::dot(Vec3(x - 1, y-1	, z),		getGradient(x0 + 1	, y0 + 1	, z0));

		float bfl = Vec3::dot(Vec3(x	, y		, z - 1),	getGradient(x0		, y0		, z0 + 1));
		float bfr = Vec3::dot(Vec3(x - 1, y		, z - 1),	getGradient(x0 + 1	, y0		, z0 + 1));

		float bbl = Vec3::dot(Vec3(x	, y		, z),		getGradient(x0		, y0		, z0));
		float bbr = Vec3::dot(Vec3(x - 1, y		, z),		getGradient(x0 + 1	, y0		, z0));

		x = PerlinNoise::fade(x);
		y = PerlinNoise::fade(y);
		z = PerlinNoise::fade(z);

		float tb = PerlinNoise::interpolate(tbl, tbr, x);
		float tf = PerlinNoise::interpolate(tfl, tfr, x);
		float bb = PerlinNoise::interpolate(bbl, bbr, x);
		float bf = PerlinNoise::interpolate(bfl, bfr, x);

		float t = PerlinNoise::interpolate(tb, tf, z);
		float b = PerlinNoise::interpolate(bb, bf, z);

		float v = PerlinNoise::interpolate(b, t, y);

		return (v + 1) / 2;
	}

	Vec3 PerlinNoise3D::getGradient(int x, int y, int z) {
		int s = 0;
		srand(seed);
		s += rand();
		srand(s + x);
		s += rand();
		srand(s + y);
		s += rand();
		srand(s + z);
		return gradientVectors[rand() % 12];
	}



	
}}