#pragma once
#include "../maths/maths.h"

namespace CariEngine { namespace Noise {
	using namespace Maths;

	class NoiseFunction2D{
	
	public:
		Vec2 offset;
		Vec2 scale;
		virtual float noise(float x, float y) = 0;
	};

	class NoiseFunction3D {

	public:
		Vec3 offset;
		Vec3 scale;
		virtual float noise(float x, float y, float z) = 0;
	};

	class NoiseFunction4D {

	public:
		Vec4 offset;
		Vec4 scale;
		virtual float noise(float x, float y, float z, float w) = 0;
	};



}}