#pragma once
#include "NoiseFunction.h"
#include "../maths/maths.h"

namespace CariEngine { namespace Noise {
	using namespace Maths;

	class PerlinNoise3D : public NoiseFunction3D{
	private:
		unsigned long seed;
		Vec3 getGradient(int x, int y, int z);
	public:
		PerlinNoise3D(unsigned long seed);
		float NoiseFunction3D::noise(float x, float y, float z);
	};

	class PerlinNoise2D : public NoiseFunction2D {
	private:
		unsigned long seed;
		Vec2 getGradient(int x, int y);
	public:
		PerlinNoise2D(unsigned long seed);
		float NoiseFunction2D::noise(float x, float y);
	};

	class PerlinNoise {
	public:
		static float interpolate(float a, float b, float w) {

			return (((1 - w) * a) + (w * b));
		}

		static float fade(float f) {
			return f * f * f * (10 + f * (-15 + f * 6));
		}
	};


}}