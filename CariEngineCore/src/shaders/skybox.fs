#version 400

in vec3 frag_texture_coords;

uniform samplerCube cubemap;

out vec4 color;

void main()
{
	color = texture(cubemap, frag_texture_coords);
}

