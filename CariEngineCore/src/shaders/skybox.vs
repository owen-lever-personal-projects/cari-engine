#version 400

layout (location = 0)
in vec3 position;

out vec3 frag_texture_coords;

uniform mat4 pr_matrix = mat4(1.0);
uniform mat4 vw_matrix = mat4(1.0);


void main()
{
	frag_texture_coords = position;
	mat4 view = mat4(mat3(vw_matrix));
	
	vec4 pos = pr_matrix * view * vec4(position, 1.0);
	gl_Position = pos.xyww;
}