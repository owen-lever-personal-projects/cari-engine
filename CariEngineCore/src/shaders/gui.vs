#version 330 core

in vec3 position;
in vec2 uv;
in vec4 color;

out vec4 frag_color;
out vec2 frag_uv;

uniform mat4 pr_matrix = mat4(1.0);
uniform mat4 vw_matrix = mat4(1.0);
uniform mat4 ml_matrix = mat4(1.0);


void main()
{
	gl_Position = pr_matrix * vec4(position.xyz, 1);

	frag_uv = uv;
	frag_color = color;

}