#version 400



out vec4 color;


in vec4 frag_color;
in vec2 frag_uv;
in vec3 frag_normal;
in vec3 frag_pos;
in float fog_visibility;
in vec4 shadow_pos;

uniform sampler2D tex;
uniform sampler2D shadowMap;

uniform vec3 viewPos;
uniform vec3 fog = vec3(1.0, 1.0, 1.0);

struct PointLight {
	vec3 position;

	float constant;
	float linear;
	float quadratic;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct DirectionalLight {
	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};


struct Material {
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float shininess;
};

uniform PointLight pointLights[32];
uniform int amountOfPointLights;

uniform DirectionalLight directionalLights[24];
uniform int amountOfDirectionalLights;

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir, Material material);
vec3 CalcDirectionalLight(DirectionalLight light, vec3 normal, vec3 fragPos, vec3 viewDir, Material material);

void main()
{
	// Creates a default material
	Material material;
	material.ambient = vec3(0.8, 0.8, 0.8);
	material.diffuse = vec3(0.7, 0.7, 0.7);
	material.specular = vec3(0.4, 0.4, 0.4);
	material.shininess = 0.8;

	// Normal vector pointing out of the polygon
	vec3 norm = vec3(normalize(frag_normal));

	// Code used for flat shading
	vec3 xTangent = dFdx( frag_pos );
    vec3 yTangent = dFdy( frag_pos );
    //norm = normalize( cross( xTangent, yTangent ) );

	// Vector pointing from camera to fragment
	vec3 viewDir = normalize(frag_pos - viewPos);

	// Loops through all lights in the scene and sums their brightness
	vec3 lightResult = vec3(0, 0, 0);
	for (int i = 0; i < amountOfPointLights; i++) {
		lightResult += CalcPointLight(pointLights[i], norm, frag_pos, viewDir, material);
	}
	for(int i = 0; i < amountOfDirectionalLights; i++){
		lightResult += CalcDirectionalLight(directionalLights[i], norm, frag_pos, viewDir, material);
	}

	// Sets the minimum amount of light to (0.05, 0.05, 0.05)
	lightResult = max(lightResult, vec3(0.05, 0.05, 0.05));

	// Mixes the polygon colour, texture colour, and light
	color = frag_color;
	color *= texture(tex, frag_uv);
	color *= vec4((lightResult).xyz, 1);
	
	// Belnds the
	//color.xyz = mix(vec3(fog.xyz), color.xyz, fog_visibility);
}

vec3 CalcDirectionalLight(DirectionalLight light, vec3 normal, vec3 fragPos, vec3 viewDir, Material material){
	
	vec3 reflectDir = reflect(-light.direction, normal);

	float diff = max(dot(-normal, light.direction), 0.0);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);

	vec3 ambient = light.ambient * material.ambient;
	vec3 diffuse = light.diffuse * diff * material.diffuse;
	vec3 specular = light.specular * spec * material.specular;

	vec3 projCoords = shadow_pos.xyz;/// shadow_pos.w;
	projCoords = projCoords * 0.5 + 0.5;
	float closestDepth = texture(shadowMap, projCoords.xy).r;
	float bias = max(0.005 * (1.0 - dot(normal, light.direction)), 0.0005);  
	//bias = 0.001;
	float currentDepth = projCoords.z - bias;
	float shadow = (currentDepth > closestDepth) ? 0.0 : 1.0;

	
	float angle = dot(-light.direction, normal);
	angle += 1;
	angle /= 2;
	
	float angleBias = 0.2f;
	
	float angleDiff = angle - texture(shadowMap, projCoords.xy).g;
	angleDiff = max(angleDiff, -angleDiff);
	//shadow = (angleDiff > angleBias) ? 0.0 : shadow;

	vec3 result = (diffuse + specular)*shadow;
	return (ambient + result);
	

}

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir, Material material) {

	float dist = length(light.position - fragPos);

	float attenuation = 1.0 / ((light.constant) + (light.linear * dist) + (light.quadratic * dist * dist));
	if(attenuation < 0.01){
		return vec3(0, 0, 0);
	}


	vec3 lightDir = normalize(fragPos - light.position);

	vec3 reflectDir = reflect(-lightDir, normal);

	float diff = max(dot(-normal, lightDir), 0.0);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);

	

	//vec3 ambient = light.ambient * vec3(texture(material.diffuse, vertUV));
	vec3 ambient = light.ambient * material.ambient;
	vec3 diffuse = light.diffuse * diff * material.diffuse;
	vec3 specular = light.specular * spec * material.specular;

	return (ambient + diffuse + specular) * attenuation;











}