#version 330 core

//layout(location = 0)
//out float depth;


in vec2 frag_uv;
in vec3 frag_normal;

uniform sampler2D tex;

uniform vec3 lightDir;

void main()
{
	vec4 color = texture(tex, frag_uv);
	if(color.a < 1)
		discard;
	float depth = gl_FragCoord.z;
	float angle = dot(-lightDir, frag_normal);
	angle += 1;
	angle /= 2;
	gl_FragColor = vec4(depth, angle, 0, 1);
}
