#version 400
// This shader shares much code with the standard fragment shader
// However it isn't being updated when changes are made to the standard shader
// The only real difference is these lines
/*
int celSteps = 4;
	
	lightResult *= celSteps;
	lightResult.r = round(lightResult.r);
	lightResult.g = round(lightResult.g);
	lightResult.b = round(lightResult.b);
	lightResult /= celSteps;
*/
// So if it stops working then just copy the code from the other shader



in vec4 frag_color;
in vec2 frag_uv;
in vec3 frag_normal;
in vec3 frag_pos;
in float fog_visibility;

out vec4 color;


uniform sampler2D tex;

uniform vec3 viewPos;
uniform vec3 fog = vec3(1.0, 1.0, 1.0);

struct PointLight {
	vec3 position;

	float constant;
	float linear;
	float quadratic;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};


struct Material {
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float shininess;
};

uniform PointLight pointLights[2048];
uniform int amountOfPointLights;

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir, Material material);

void main()
{

	Material material;
	material.ambient = vec3(0.8, 0.8, 0.8);
	material.diffuse = vec3(0.7, 0.7, 0.7);
	material.specular = vec3(0.4, 0.4, 0.4);
	material.shininess = 0.8;


	vec3 norm = vec3(normalize(frag_normal));
	vec3 viewDir = normalize(frag_pos - viewPos);

	
	vec3 lightResult = vec3(0, 0, 0);
	for (int i = 0; i < amountOfPointLights; i++) {
		lightResult += CalcPointLight(pointLights[i], norm, frag_pos, viewDir, material);
	}

	lightResult = max(lightResult, vec3(0.2, 0.2, 0.2));
	int celSteps = 4;
	
	lightResult *= celSteps;
	lightResult.r = round(lightResult.r);
	lightResult.g = round(lightResult.g);
	lightResult.b = round(lightResult.b);
	lightResult /= celSteps;

	color = frag_color;
	color *= texture(tex, frag_uv);
	color *= vec4((lightResult).xyz, 1);


	//color = vec4(edge, edge, edge, 1);

	if (color.a > 0.01) {
		//color = mix(vec4(fog.xyz, 1.0), color, fog_visibility);
	}
}

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir, Material material) {

	float dist = length(light.position - fragPos);

	float attenuation = 1.0 / ((light.constant) + (light.linear * dist) + (light.quadratic * dist * dist));
	if(attenuation < 0.01){
		return vec3(0, 0, 0);
	}


	vec3 lightDir = normalize(fragPos - light.position);

	vec3 reflectDir = reflect(-lightDir, normal);

	float diff = max(dot(-normal, lightDir), 0.0);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);

	

	//vec3 ambient = light.ambient * vec3(texture(material.diffuse, vertUV));
	vec3 ambient = light.ambient * material.ambient;
	vec3 diffuse = light.diffuse * diff * material.diffuse;
	vec3 specular = light.specular * spec * material.specular;

	return (ambient + diffuse + specular) * attenuation;
	//return vec3(1, 1, 1);
}
