#version 400





in vec4 frag_color;
in vec2 frag_uv;

out vec4 color;


uniform sampler2D tex;

void main()
{
	color = frag_color;
	color *= texture(tex, frag_uv);
}
