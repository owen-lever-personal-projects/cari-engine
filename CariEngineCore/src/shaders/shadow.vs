#version 330 core

layout (location = 0)
in vec4 position;
layout (location = 1)
in vec2 uv;
layout (location = 2)
in vec3 normal;
layout (location = 3)
in vec4 color;

uniform mat4 pr_matrix = mat4(1.0);
uniform mat4 vw_matrix = mat4(1.0);
uniform mat4 ml_matrix = mat4(1.0);

out vec2 frag_uv;
out vec3 frag_normal;

void main()
{
	gl_Position = pr_matrix * vw_matrix * ml_matrix * position;
	frag_uv = uv;
	frag_normal = normal;
}