#version 330 core

layout (location = 0)
in vec4 position;
layout (location = 1)
in vec2 uv;
layout (location = 2)
in vec3 normal;
layout (location = 3)
in vec4 color;

out vec4 frag_color;
out vec2 frag_uv;
out vec3 frag_normal;
out vec3 frag_pos;

out vec4 shadow_pos;

out float fog_visibility;

uniform mat4 pr_matrix;
uniform mat4 vw_matrix = mat4(1.0);
uniform mat4 ml_matrix = mat4(1.0);

uniform mat4 shadow_pr_matrix = mat4(1.0);
uniform mat4 shadow_vw_matrix = mat4(1.0);

uniform float fog_density = 0.002;
uniform float fog_gradient = 1.5;

void main()
{
	frag_uv = uv;
	
	vec4 posRelativeToCam = vw_matrix * ml_matrix * position;
	vec4 pos = pr_matrix * posRelativeToCam;

	gl_Position = pos;


	frag_pos = vec3(ml_matrix * position);
	frag_color = color;
	frag_normal = mat3(transpose(inverse(ml_matrix))) * normal;

	shadow_pos = shadow_pr_matrix * shadow_vw_matrix * ml_matrix * position;

	float distance = length(posRelativeToCam.xyz);
	fog_visibility = exp(-pow((distance * fog_density), fog_gradient));
	fog_visibility = clamp(fog_visibility, 0.0, 1.0);
}