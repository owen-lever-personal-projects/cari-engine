#pragma once
#include "../../maths/maths.h"


namespace CariEngine { namespace Graphics {
	using namespace Maths;


	struct PointLight {

		PointLight() : PointLight(Vec3(0, 0, 0)){

		}

		PointLight(Vec3 position) : PointLight(position, Vec3(1, 1, 1)) {

		}

		PointLight(Vec3 position, Vec3 color) {
			this->position = position;
			//this->ambient = color;
			this->diffuse = color;
			//this->specular = color;

			this->constant = 300;
			this->linear = 0;
			this->quadratic = 0;

		}

		Vec3 position;

		float constant;
		float linear;
		float quadratic;

		Vec3 ambient;
		Vec3 diffuse;
		Vec3 specular;
	};

	struct DirectionalLight {
		
		DirectionalLight(Vec3 direction = Vec3(0, 0, 0), Vec3 color = Vec3(1, 1, 1)) {
			this->direction = direction;
			this->ambient = color;
			this->diffuse = color;
			this->specular = color;
		}

		Vec3 direction;

		Vec3 ambient;
		Vec3 diffuse;
		Vec3 specular;
	};








}}