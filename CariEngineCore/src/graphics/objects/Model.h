#pragma once
#include "../shaders/Shader.h"
#include <vector>
#include "Mesh.h"
#include <string>
#include <ASSIMP/mesh.h>
#include <ASSIMP/material.h>
#include <ASSIMP/scene.h>


namespace CariEngine { namespace Graphics {

	class Model {
	public:
		Model() {
		}
		Model(const char* path, Shader* shader);
		Model(Mesh mesh, Shader* shader);

		void draw();

	private:
		Shader* shader;
		std::vector<Mesh> meshes;
		std::string directory;

		void loadModel(const char* path);
		void processNode(aiNode* node, const aiScene* scene);
		Mesh processMesh(aiMesh* mesh, const aiScene* scene);
		std::vector<Texture*> loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName);


	};




}}