#pragma once
#include "../../maths/maths.h"

namespace CariEngine { namespace Graphics {
	using namespace Maths;

	struct Vertex2D {
		Vec3 position;
		Vec4 color;
		Vec2 uv;
	};
	struct Vertex3D {
		Vec3 position;
		Vec2 uv;
		Vec4 color;
		Vec3 normal;

		Vertex3D() {
			this->position = Vec3(0, 0, 0);
			this->normal = Vec3(1, 1, 1);
			this->uv = Vec2(0, 0);
			this->color = Vec4(1, 1, 1, 1);
		}
		Vertex3D(Vec3 position, Vec3 normal, Vec2 uv) {
			this->position = position;
			this->normal = normal;
			this->uv = uv;
			this->color = Vec4(1, 1, 1, 1);
		}
	};
	struct Vertex3DAnimated {
		Vec3 position;
		Vec2 uv;
		Vec4 color;
		Vec3 normal;
		Vec4 jointIDs;
		Vec4 jointWights;

		Vertex3DAnimated(Vec3 position, Vec3 normal, Vec2 uv, Vec4 jointsIDs, Vec4 JointWeights) {
			this->position = position;
			this->normal = normal;
			this->uv = uv;
			this->jointIDs = jointIDs;
			this->jointWights = JointWeights;
			this->color = Vec4(1, 1, 1, 1);
		}
	};


}}