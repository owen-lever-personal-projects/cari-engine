#pragma once
#include "Mesh.h"
#include "../../maths/maths.h"
#include "../textures/Texture.h"
#include "../../noise/NoiseFunction.h"

namespace CariEngine { namespace Graphics {
	using namespace Noise;

	class MeshTools {
	private:
		MeshTools();

	public:
		static Mesh Box(Vec3 size, Texture* texture);
		static Mesh Plane(Vec2 size, Texture* texture);
		static Mesh Plane(Vec2 size, NoiseFunction2D* noise, Texture* texture);
		static Mesh Volume(Vec3 size, NoiseFunction3D* noise, Texture* texture);
		static Mesh NormalisedCube(int detail, Texture* texture);
		static Mesh UVSphere(int latLines, int longLines, Texture* texture);
	};




}}