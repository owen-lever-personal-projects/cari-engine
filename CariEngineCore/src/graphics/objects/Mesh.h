#pragma once
#include <vector>
#include "VertexData.h"
#include <GL/glew.h>
#include "../textures/Texture.h"
#include "../shaders/Shader.h"
namespace CariEngine { namespace Graphics {

	class Mesh {
	public:
		std::vector<Vertex3D> vertices;
		std::vector<GLuint> indices;
		std::vector<Texture*> textures;

		Mesh(std::vector<Vertex3D> vertices, std::vector<GLuint> indices, std::vector<Texture*> textures);
		void draw(Shader* shader);
		void setupMesh(Shader* shader);
		void recalculateNormals();

	private:
		bool setup;
		GLuint vao, vbo, ebo;
	};




}}
