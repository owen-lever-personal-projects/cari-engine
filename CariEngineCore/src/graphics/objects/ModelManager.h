#pragma once
#include <map>
#include <string>
#include "Model.h"
#include "Mesh.h"

namespace CariEngine {
	namespace Graphics {

		class ModelManager {
			static std::map<std::string, Model> models;

			static Shader* shader;
		public:
			static void init(Shader* shader);


			static Model* getModel(std::string filePath);
			static Model* getModel(Mesh mesh);


		};



	}
}