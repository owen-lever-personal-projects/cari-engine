#include "MeshOld.h"


#include "../../utils/FileReader.h"
#include <string>
#include <fstream>


using namespace CariEngine;
using namespace Graphics;
/*
void Mesh::draw() {
	texture->bind();
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
	glEnableVertexAttribArray(5);
	
	glDrawArrays(GL_TRIANGLES, 0, verticesCount);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
	glDisableVertexAttribArray(4);
	glDisableVertexAttribArray(5);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	texture->unbind();

}




Mesh::Mesh(const char* filePath, shader* shader) {
	texture = new Texture("src/textures/Portal_Companion_Cube.png");
	
	std::ifstream data(filePath);

	std::string line;
	std::string delim = " ";

	std::vector<Vec3> tmpVertex;
	std::vector<Triangle> tmpTris;
	std::vector<Vec2> tmpUV;
	std::vector<Vec3> tmpNormal;

	while (std::getline(data, line)) {
		
		if (line.find("v ") == 0) {
			Vec3 vertex;

			std::vector<std::string> data = split(line, delim);
			vertex.x = atof(data[1].c_str());
			vertex.y = atof(data[2].c_str());
			vertex.z = atof(data[3].c_str());
			tmpVertex.push_back(vertex);

		}

		if (line.find("vt ") == 0) {
			Vec2 uv;
			
			std::vector<std::string> data = split(line, delim);
			uv.x = atof(data[1].c_str());
			uv.y = atof(data[2].c_str());
			tmpUV.push_back(uv);
		}

		if (line.find("vn ") == 0) {
			Vec3 normal;

			std::vector<std::string> data = split(line, delim);
			normal.x = atof(data[1].c_str());
			normal.y = atof(data[2].c_str());
			normal.z = atof(data[3].c_str());
			tmpNormal.push_back(normal);
		}

		if (line.find("f ") == 0) {
			Triangle tri;
			std::vector<std::string> data = split(line, delim);
			for (int i = 0; i < data.size() - 3; i++) {

				std::vector<std::string> v1 = split(data[1], "/");

				tri.vertex1		= atoi(v1[0].c_str()) - 1;
				tri.uv1			= atoi(v1[1].c_str()) - 1;
				tri.normal1		= atoi(v1[2].c_str()) - 1;

				std::vector<std::string> v2 = split(data[i+2], "/");
				tri.vertex2		= atoi(v2[0].c_str()) - 1;
				tri.uv2			= atoi(v2[1].c_str()) - 1;
				tri.normal2		= atoi(v2[2].c_str()) - 1;

				std::vector<std::string> v3 = split(data[i+3], "/");
				tri.vertex3		= atoi(v3[0].c_str()) - 1;
				tri.uv3			= atoi(v3[1].c_str()) - 1;
				tri.normal3		= atoi(v3[2].c_str()) - 1;
				tmpTris.push_back(tri);
			}
		}
	
	}
	Vec3 offs;
	int amoutOfVerts = 0;
	for (int i = 0; i < tmpTris.size(); i++) {
		
		offs.subtract(tmpVertex[tmpTris[i].vertex1]);
		offs.subtract(tmpVertex[tmpTris[i].vertex2]);
		offs.subtract(tmpVertex[tmpTris[i].vertex3]);


		amoutOfVerts+=3;
	}
	offs = offs / amoutOfVerts;
	for (int i = 0; i < tmpVertex.size(); i++) {
		tmpVertex[i].add(offs);
		
	}


	std::vector<VertexData3D> vertices;
	for (int i = 0; i < tmpTris.size(); i++) {
		
		
		VertexData3D a;
		a.position = tmpVertex[tmpTris[i].vertex1];
		a.color = Vec4(1, 1, 1, 1);
		a.uv = tmpUV[tmpTris[i].uv1];
		a.normal = tmpNormal[tmpTris[i].normal1];
		vertices.push_back(a);

		VertexData3D b;
		b.position = tmpVertex[tmpTris[i].vertex2];
		b.color = Vec4(1, 1, 1, 1);
		b.uv = tmpUV[tmpTris[i].uv2];
		b.normal = tmpNormal[tmpTris[i].normal2];
		vertices.push_back(b);

		VertexData3D c;
		c.position = tmpVertex[tmpTris[i].vertex3];
		c.color = Vec4(1, 1, 1, 1);
		c.uv = tmpUV[tmpTris[i].uv3];
		c.normal = tmpNormal[tmpTris[i].normal3];
		vertices.push_back(c);

	}
	
	this->verticesCount = tmpTris.size() * 3;
	
	//glGenVertexArrays(1, &vao);
	//glBindVertexArray(vao);


	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	
	
	glBufferData(GL_ARRAY_BUFFER, sizeof(VertexData3D) * vertices.size(), &vertices[0], GL_STATIC_DRAW);
	
	
	GLuint position = shader->getAttribLocation("position");
	glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData3D), (const GLvoid*)offsetof(VertexData3D, VertexData3D::position));
	glEnableVertexAttribArray(position);
	
	GLuint color = shader->getAttribLocation("color");
	glVertexAttribPointer(color, 4, GL_FLOAT, GL_TRUE, sizeof(VertexData3D), (const GLvoid*)offsetof(VertexData3D, VertexData3D::color));
	glEnableVertexAttribArray(color);

	GLuint uv = shader->getAttribLocation("uv");
	glVertexAttribPointer(uv, 2, GL_FLOAT, GL_TRUE, sizeof(VertexData3D), (const GLvoid*)offsetof(VertexData3D, VertexData3D::uv));
	glEnableVertexAttribArray(uv);

	GLuint normal = shader->getAttribLocation("normal");
	glVertexAttribPointer(normal, 3, GL_FLOAT, GL_TRUE, sizeof(VertexData3D), (const GLvoid*)offsetof(VertexData3D, VertexData3D::normal));
	glEnableVertexAttribArray(normal);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);

}
*/