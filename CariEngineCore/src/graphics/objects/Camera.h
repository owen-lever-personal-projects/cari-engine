#pragma once
#include "../../maths/maths.h"
#include <math.h>

namespace CariEngine { namespace Graphics {
	using namespace Maths;
	class Camera {

		public:
		Vec3 position;

		Camera(Vec3 pos) {
			this->position = pos;
		}

		Vec3 getPosition() {
			return position;
		}

		virtual Mat4 getView() = 0;
		virtual Vec3 getDirection() = 0;

		static Vec3 calcTarget(float hRotation, float vRotation) {
			hRotation = toRadians(hRotation);
			vRotation = toRadians(vRotation);


			float x = sin(hRotation);
			float z = -cos(hRotation);

			float y = sin(vRotation);
			float m = cos(vRotation);

			x *= m;
			z *= m;

			return Vec3(x, y, z);
		}
	};
	/*
	Vec3 Vec3::getDirection(float hRotation, float vRotation) {
			float x = cos(hRotation);
			float z = sin(hRotation);

			float y = sin(vRotation);
			float m = cos(vRotation);

			x *= m;
			z *= m;

			return Vec3(x, y, z);
		}
	*/
	class FpsCam : public Camera {
		float vRotation;
		float hRotation;

		public:

		FpsCam(Vec3 pos) : Camera(pos) {
			vRotation = 0;
			hRotation = 0;
		}


		void setRotation(float hRotation, float vRotation) {
			this->hRotation = hRotation;
			this->vRotation = vRotation;
		}


		Mat4 getView() override{
			//return Mat4::translation(position * -1).multiply(Mat4::rotation(0, toRadians(vRotation), toRadians(hRotation)));
			Vec3 target = getDirection() + position;
			Vec3 up = calcTarget(hRotation, vRotation + 90);
			return Mat4::lookAt(position, target, up);
		}

		Vec3 getDirection() override{
			return calcTarget(hRotation, vRotation);
		}

		Vec3 getRight() {
			return calcTarget(hRotation + 90, 0);
		}

		Vec3 getLeft() {
			return calcTarget(hRotation - 90, 0);
		}

		Vec3 getBack() {
			return getDirection() * -1;
		}

		Vec3 getUp() {
			return Vec3(0, 1, 0);
		}

		Vec3 getDown() {
			return Vec3(0, -1, 0);
		}

	};

	class SpaceCam : public Camera {
		Vec3 up;
		Vec3 forward;

		public:
		//Camera::position;

		SpaceCam(Vec3 pos) : Camera(pos) {
			up = Vec3(0, 1, 0);
			forward = Vec3(0, 0, -1);
		}

		Mat4 getView() override{
			return Mat4::lookAt(position, position + forward, up);
		}

		void addYaw(float angle) {
			angle = toRadians(angle);
			forward = Vec3::rotate(forward, up, -angle);
		}

		void addPitch(float angle) {
			angle = toRadians(angle);
			Vec3 left = Vec3::cross(up, forward);
			forward = Vec3::rotate(forward, left, angle);
			up = Vec3::rotate(up, left, angle);
		}

		void addRoll(float angle) {
			angle = toRadians(angle);
		}



		Vec3 getDirection() override {
			return forward;
		}

		Vec3 getRight() {
			return Vec3::cross(forward, up);
		}

		Vec3 getLeft() {
			return Vec3::cross(up, forward);
		}

		Vec3 getBack() {
			return forward * -1;
		}

		Vec3 getUp() {
			return up;
		}

		Vec3 getDown() {
			return up * -1;
		}

	};
}}