#include "Mesh.h"
#include <map>

namespace CariEngine { namespace Graphics {

	Mesh::Mesh(std::vector<Vertex3D> vertices, std::vector<GLuint> indices, std::vector<Texture*> textures) {
		this->vertices = vertices;
		this->indices = indices;
		this->textures = textures;
		setup = false;

	}

	void Mesh::recalculateNormals() {
		if (setup)
			return;
		//We can change this later, but for now normals cannot be recalculated once the model is bound

		int indicesCount = indices.size();

		for (int i = 0; i < vertices.size(); i++) {
			vertices[i].normal = Vec3(0, 0, 0);
		}

		for (int i = 0; i < indicesCount; i+=3) {
			Vec3 a = vertices[indices[i + 0]].position;
			Vec3 b = vertices[indices[i + 1]].position;
			Vec3 c = vertices[indices[i + 2]].position;
			Vec3 normal = Vec3::cross((b-a).normalise(), (c-a).normalise());
			
			vertices[indices[i + 0]].normal.add(normal);
			vertices[indices[i + 1]].normal.add(normal);
			vertices[indices[i + 2]].normal.add(normal);
			
		}
		for (int i = 0; i < vertices.size(); i++) {
			vertices[i].normal.normalise();
		}

	}

	void Mesh::setupMesh(Shader* shader) {
		if (setup) {
			return;
		}
		setup = true;

		glGenVertexArrays(1, &vao);
		glGenBuffers(1, &vbo);
		glGenBuffers(1, &ebo);

		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);

		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex3D), &vertices[0], GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);

		glEnableVertexAttribArray(0);
		GLint pos = shader->getAttribLocation("position");
		glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex3D), (void*)offsetof(Vertex3D, position));

		glEnableVertexAttribArray(1);
		GLint uv = shader->getAttribLocation("uv");
		glVertexAttribPointer(uv, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex3D), (void*)offsetof(Vertex3D, uv));

		glEnableVertexAttribArray(2);
		GLint color = shader->getAttribLocation("color");
		glVertexAttribPointer(color, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex3D), (void*)offsetof(Vertex3D, color));

		glEnableVertexAttribArray(3);
		GLint normal = shader->getAttribLocation("normal");
		glVertexAttribPointer(normal, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex3D), (void*)offsetof(Vertex3D, normal));









		glBindVertexArray(0);
	}

	void Mesh::draw(Shader* shader) {
		for (int i = 0; i < textures.size(); i++) {
			glActiveTexture(GL_TEXTURE0 + i);
			textures[i]->bind();
		}
		glActiveTexture(GL_TEXTURE0);


		glBindVertexArray(vao);
		glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}


}}