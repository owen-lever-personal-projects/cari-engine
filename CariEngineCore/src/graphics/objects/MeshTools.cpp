#include "MeshTools.h"
#include <vector>
#include "VertexData.h"
#include <GL/glew.h>
#include <map>
namespace CariEngine {
	namespace Graphics {

		Mesh MeshTools::NormalisedCube(int detail, Texture* texture) {

			detail++;
			if (detail < 2) detail = 2;
			std::vector<Texture*> textures;
			std::vector<Vertex3D> vertices;
			std::vector<GLuint> indices;
			
			textures.push_back(texture);


			// Generates a cube by creating 6 planes, one in each direction
			//The distance between each vertex on the cube
			float dist = 2 / (float)(detail-1);

			// Top face
			for (int i = 0; i < detail; i++) { for (int j = 0; j < detail; j++) {
					vertices.push_back(Vertex3D(Vec3(-1 + j*dist, 1, -1 + i*dist), Vec3(0), Vec2(((float)j)/(detail-1), ((float)-i)/(detail-1))));
			}}

			// Bottom face
			for (int i = 0; i < detail; i++) { for (int j = 0; j < detail; j++) {
					vertices.push_back(Vertex3D(Vec3(-1 + j*dist, -1, 1 - i*dist), Vec3(0), Vec2(((float)j)/(detail-1), ((float)-i)/(detail-1))));
			}}

			// Right face
			for (int i = 0; i < detail; i++) { for (int j = 0; j < detail; j++) {
					vertices.push_back(Vertex3D(Vec3(1, 1 - i * dist, 1 - j*dist), Vec3(0), Vec2(((float)j)/(detail-1), ((float)-i)/(detail-1))));
			}}

			// Left face
			for (int i = 0; i < detail; i++) { for (int j = 0; j < detail; j++) {
					vertices.push_back(Vertex3D(Vec3(-1, 1 - i*dist, -1 + j*dist), Vec3(0), Vec2(((float)j)/(detail-1), ((float)-i)/(detail-1))));
			}}

			// Front face
			for (int i = 0; i < detail; i++) { for (int j = 0; j < detail; j++) {
					vertices.push_back(Vertex3D(Vec3(-1 + j*dist, 1 - i*dist, 1), Vec3(0), Vec2(((float)j)/(detail-1), ((float)-i)/(detail-1))));
			}}

			// Back face
			for (int i = 0; i < detail; i++) { for (int j = 0; j < detail; j++) {
					vertices.push_back(Vertex3D(Vec3(1 - j*dist, 1 - i*dist, -1), Vec3(0), Vec2(((float)j)/(detail-1), ((float)-i)/(detail-1))));
			}}

			// Normalises each vertex, transforming the box to a sphere
			for (auto it = vertices.begin(); it != vertices.end(); it++) {
				Vertex3D* vert = &*it;
				vert->position.normalise();
				vert->normal = vert->position;
			}

			// Fills in the indices for each triangle
			for (int i = 0; i < 6; i++) {
				for (int j = 0; j < detail-1; j++) {
					for (int k = 0; k < detail-1; k++) {
						int offSet = detail * detail *  i;

						int t = (k + 0) * (detail);
						int b = (k + 1) * (detail);
						int l = j;
						int r = j + 1;

						indices.push_back(b + l + offSet);
						indices.push_back(b + r + offSet);
						indices.push_back(t + l + offSet);

						indices.push_back(t + r + offSet);
						indices.push_back(t + l + offSet);
						indices.push_back(b + r + offSet);
					}
				}
			}

			
			return Mesh(vertices, indices, textures);
		}

		Mesh MeshTools::UVSphere(int latLines, int longLines, Texture* texture) {

			std::vector<Texture*> textures;
			std::vector<Vertex3D> vertices;
			std::vector<GLuint> indices;

			textures.push_back(texture);

			int sizeX = longLines + 1;
			int sizeY = latLines + 2;

			for (int y = 0; y < sizeY; y++) {
				for (int x = 0; x < sizeX; x++) {
					float vAngle = ((y * pi) / (sizeY-1)) - pi/2;
					float hAngle = (x * pi * 2) / (sizeX-1);
					Vec3 pos = Vec3::getDirection(hAngle, vAngle);
					Vec2 uv = Vec2(1 - (float)x / (sizeX-1), (float)y / (sizeY-1));
					vertices.push_back(Vertex3D(pos, pos, uv));

				}
			}

			for (int j = 0; j < sizeY-1; j++) {
				for (int i = 0; i < sizeX-1; i++) {

					int t = (j + 0) * (sizeX);
					int b = (j + 1) * (sizeX);
					int l = i;
					int r = i + 1;

					indices.push_back(b + l);
					indices.push_back(b + r);
					indices.push_back(t + l);

					indices.push_back(t + r);
					indices.push_back(t + l);
					indices.push_back(b + r);
				}
			}

			return Mesh(vertices, indices, textures);
		}
		


		Mesh MeshTools::Plane(Vec2 size, Texture* texture) {
			std::vector<Texture*> textures;
			std::vector<Vertex3D> vertices;
			std::vector<GLuint> indices;

			textures.push_back(texture);


			for (int j = 0; j <= size.y; j++) {
				for (int i = 0; i <= size.x; i++) {
					vertices.push_back(Vertex3D(Vec3(i, 0, j), Vec3(0, 1, 0), Vec2(((float)i) / 32, ((float)-j) / 32)));
				}
			}
			for (int i = 0; i < size.x; i++) {
				for (int j = 0; j < size.y; j++) {
					int t = (j + 0) * (size.x + 1);
					int b = (j + 1) * (size.x + 1);
					int l = i;
					int r = i + 1;

					indices.push_back(b + l);
					indices.push_back(b + r);
					indices.push_back(t + l);

					indices.push_back(t + r);
					indices.push_back(t + l);
					indices.push_back(b + r);
				}
			}
			Mesh result = Mesh(vertices, indices, textures);
			//result.recalculateNormals();
			return result;
		}

		Mesh MeshTools::Box(Vec3 size, Texture* texture) {
			std::vector<Texture*> textures;
			std::vector<Vertex3D> vertices;
			std::vector<GLuint> indices;

			textures.push_back(texture);
			size /= 2;

			for (int i = 0; i < 6; i++) {
				indices.push_back(i * 4);
				indices.push_back(i * 4 + 1);
				indices.push_back(i * 4 + 2);

				indices.push_back(i * 4 + 3);
				indices.push_back(i * 4 + 2);
				indices.push_back(i * 4 + 1);
			}

			//Top face
			vertices.push_back(Vertex3D(Vec3(-size.x, size.y, -size.z), Vec3(0, 1, 0), Vec2(0, 0)));
			vertices.push_back(Vertex3D(Vec3(-size.x, size.y, size.z), Vec3(0, 1, 0), Vec2(0, -size.z / 32)));
			vertices.push_back(Vertex3D(Vec3(size.x, size.y, -size.x), Vec3(0, 1, 0), Vec2(size.x / 32, 0)));
			vertices.push_back(Vertex3D(Vec3(size.x, size.y, size.z), Vec3(0, 1, 0), Vec2(size.x / 32, -size.z / 32)));

			//Bottom face
			vertices.push_back(Vertex3D(Vec3(-size.x, -size.y, -size.z), Vec3(0, -1, 0), Vec2(0, -size.z / 32)));
			vertices.push_back(Vertex3D(Vec3(size.x, -size.y, -size.z), Vec3(0, -1, 0), Vec2(size.x / 32, -size.z / 32)));
			vertices.push_back(Vertex3D(Vec3(-size.x, -size.y, size.z), Vec3(0, -1, 0), Vec2(0, 0)));
			vertices.push_back(Vertex3D(Vec3(size.x, -size.y, size.z), Vec3(0, -1, 0), Vec2(size.x / 32, 0)));

			//Right face
			vertices.push_back(Vertex3D(Vec3(size.x, -size.y, -size.z), Vec3(1, 0, 0), Vec2(size.z / 32, -size.y / 32)));
			vertices.push_back(Vertex3D(Vec3(size.x, size.y, -size.z), Vec3(1, 0, 0), Vec2(size.z / 32, 0)));
			vertices.push_back(Vertex3D(Vec3(size.x, -size.y, size.z), Vec3(1, 0, 0), Vec2(0, -size.y / 32)));
			vertices.push_back(Vertex3D(Vec3(size.x, size.y, size.z), Vec3(1, 0, 0), Vec2(0, 0)));

			//Left face
			vertices.push_back(Vertex3D(Vec3(-size.x, -size.y, -size.z), Vec3(-1, 0, 0), Vec2(0, -size.y / 32)));
			vertices.push_back(Vertex3D(Vec3(-size.x, -size.y, size.z), Vec3(-1, 0, 0), Vec2(size.z / 32, -size.y / 32)));
			vertices.push_back(Vertex3D(Vec3(-size.x, size.y, -size.z), Vec3(-1, 0, 0), Vec2(0, 0)));
			vertices.push_back(Vertex3D(Vec3(-size.x, size.y, size.z), Vec3(-1, 0, 0), Vec2(size.z / 32, 0)));

			//Front face
			vertices.push_back(Vertex3D(Vec3(-size.x, -size.y, size.z), Vec3(0, 0, 1), Vec2(0, -size.y / 32)));
			vertices.push_back(Vertex3D(Vec3(size.x, -size.y, size.z), Vec3(0, 0, 1), Vec2(size.x / 32, -size.y / 32)));
			vertices.push_back(Vertex3D(Vec3(-size.x, size.y, size.z), Vec3(0, 0, 1), Vec2(0, 0)));
			vertices.push_back(Vertex3D(Vec3(size.x, size.y, size.z), Vec3(0, 0, 1), Vec2(size.x / 32, 0)));

			//Back face
			vertices.push_back(Vertex3D(Vec3(-size.x, -size.y, -size.z), Vec3(0, 0, -1), Vec2(size.x / 32, -size.y / 32)));
			vertices.push_back(Vertex3D(Vec3(-size.x, size.y, -size.z), Vec3(0, 0, -1), Vec2(size.x / 32, 0)));
			vertices.push_back(Vertex3D(Vec3(size.x, -size.y, -size.z), Vec3(0, 0, -1), Vec2(0, -size.y / 32)));
			vertices.push_back(Vertex3D(Vec3(size.x, size.y, -size.z), Vec3(0, 0, -1), Vec2(0, 0)));
			return Mesh(vertices, indices, textures);

		}

		Mesh MeshTools::Plane(Vec2 size, NoiseFunction2D* noise, Texture* texture) {
			std::vector<Texture*> textures;
			std::vector<Vertex3D> vertices;
			std::vector<GLuint> indices;

			
			textures.push_back(texture);
			for (int j = 0; j <= size.y; j++) {
				for (int i = 0; i <= size.x; i++) {
					float height = noise->noise(i / size.x, j / size.y);

					Vec3 pos = Vec3(i, 200 * height, j);
					vertices.push_back(Vertex3D(pos, Vec3(0, 1, 0), Vec2(0, height)));
				}
			}
			for (int i = 0; i < size.x; i++) {
				for (int j = 0; j < size.y; j++) {
					int t = (j + 0) * (size.x + 1);
					int b = (j + 1) * (size.x + 1);
					int l = i;
					int r = i + 1;

					indices.push_back(b + l);
					indices.push_back(b + r);
					indices.push_back(t + l);

					indices.push_back(t + r);
					indices.push_back(t + l);
					indices.push_back(b + r);


				}
			}

			Mesh result = Mesh(vertices, indices, textures);
			result.recalculateNormals();
			return result;
		}

		int triTable[256][16] =
		{ {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{0, 1, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{1, 8, 3, 9, 8, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{0, 8, 3, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{9, 2, 10, 0, 2, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{2, 8, 3, 2, 10, 8, 10, 9, 8, -1, -1, -1, -1, -1, -1, -1},
		{3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{0, 11, 2, 8, 11, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{1, 9, 0, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{1, 11, 2, 1, 9, 11, 9, 8, 11, -1, -1, -1, -1, -1, -1, -1},
		{3, 10, 1, 11, 10, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{0, 10, 1, 0, 8, 10, 8, 11, 10, -1, -1, -1, -1, -1, -1, -1},
		{3, 9, 0, 3, 11, 9, 11, 10, 9, -1, -1, -1, -1, -1, -1, -1},
		{9, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{4, 3, 0, 7, 3, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{0, 1, 9, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{4, 1, 9, 4, 7, 1, 7, 3, 1, -1, -1, -1, -1, -1, -1, -1},
		{1, 2, 10, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{3, 4, 7, 3, 0, 4, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1},
		{9, 2, 10, 9, 0, 2, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1},
		{2, 10, 9, 2, 9, 7, 2, 7, 3, 7, 9, 4, -1, -1, -1, -1},
		{8, 4, 7, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{11, 4, 7, 11, 2, 4, 2, 0, 4, -1, -1, -1, -1, -1, -1, -1},
		{9, 0, 1, 8, 4, 7, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1},
		{4, 7, 11, 9, 4, 11, 9, 11, 2, 9, 2, 1, -1, -1, -1, -1},
		{3, 10, 1, 3, 11, 10, 7, 8, 4, -1, -1, -1, -1, -1, -1, -1},
		{1, 11, 10, 1, 4, 11, 1, 0, 4, 7, 11, 4, -1, -1, -1, -1},
		{4, 7, 8, 9, 0, 11, 9, 11, 10, 11, 0, 3, -1, -1, -1, -1},
		{4, 7, 11, 4, 11, 9, 9, 11, 10, -1, -1, -1, -1, -1, -1, -1},
		{9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{9, 5, 4, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{0, 5, 4, 1, 5, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{8, 5, 4, 8, 3, 5, 3, 1, 5, -1, -1, -1, -1, -1, -1, -1},
		{1, 2, 10, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{3, 0, 8, 1, 2, 10, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1},
		{5, 2, 10, 5, 4, 2, 4, 0, 2, -1, -1, -1, -1, -1, -1, -1},
		{2, 10, 5, 3, 2, 5, 3, 5, 4, 3, 4, 8, -1, -1, -1, -1},
		{9, 5, 4, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{0, 11, 2, 0, 8, 11, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1},
		{0, 5, 4, 0, 1, 5, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1},
		{2, 1, 5, 2, 5, 8, 2, 8, 11, 4, 8, 5, -1, -1, -1, -1},
		{10, 3, 11, 10, 1, 3, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1},
		{4, 9, 5, 0, 8, 1, 8, 10, 1, 8, 11, 10, -1, -1, -1, -1},
		{5, 4, 0, 5, 0, 11, 5, 11, 10, 11, 0, 3, -1, -1, -1, -1},
		{5, 4, 8, 5, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1},
		{9, 7, 8, 5, 7, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{9, 3, 0, 9, 5, 3, 5, 7, 3, -1, -1, -1, -1, -1, -1, -1},
		{0, 7, 8, 0, 1, 7, 1, 5, 7, -1, -1, -1, -1, -1, -1, -1},
		{1, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{9, 7, 8, 9, 5, 7, 10, 1, 2, -1, -1, -1, -1, -1, -1, -1},
		{10, 1, 2, 9, 5, 0, 5, 3, 0, 5, 7, 3, -1, -1, -1, -1},
		{8, 0, 2, 8, 2, 5, 8, 5, 7, 10, 5, 2, -1, -1, -1, -1},
		{2, 10, 5, 2, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1},
		{7, 9, 5, 7, 8, 9, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1},
		{9, 5, 7, 9, 7, 2, 9, 2, 0, 2, 7, 11, -1, -1, -1, -1},
		{2, 3, 11, 0, 1, 8, 1, 7, 8, 1, 5, 7, -1, -1, -1, -1},
		{11, 2, 1, 11, 1, 7, 7, 1, 5, -1, -1, -1, -1, -1, -1, -1},
		{9, 5, 8, 8, 5, 7, 10, 1, 3, 10, 3, 11, -1, -1, -1, -1},
		{5, 7, 0, 5, 0, 9, 7, 11, 0, 1, 0, 10, 11, 10, 0, -1},
		{11, 10, 0, 11, 0, 3, 10, 5, 0, 8, 0, 7, 5, 7, 0, -1},
		{11, 10, 5, 7, 11, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{0, 8, 3, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{9, 0, 1, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{1, 8, 3, 1, 9, 8, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1},
		{1, 6, 5, 2, 6, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{1, 6, 5, 1, 2, 6, 3, 0, 8, -1, -1, -1, -1, -1, -1, -1},
		{9, 6, 5, 9, 0, 6, 0, 2, 6, -1, -1, -1, -1, -1, -1, -1},
		{5, 9, 8, 5, 8, 2, 5, 2, 6, 3, 2, 8, -1, -1, -1, -1},
		{2, 3, 11, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{11, 0, 8, 11, 2, 0, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1},
		{0, 1, 9, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1},
		{5, 10, 6, 1, 9, 2, 9, 11, 2, 9, 8, 11, -1, -1, -1, -1},
		{6, 3, 11, 6, 5, 3, 5, 1, 3, -1, -1, -1, -1, -1, -1, -1},
		{0, 8, 11, 0, 11, 5, 0, 5, 1, 5, 11, 6, -1, -1, -1, -1},
		{3, 11, 6, 0, 3, 6, 0, 6, 5, 0, 5, 9, -1, -1, -1, -1},
		{6, 5, 9, 6, 9, 11, 11, 9, 8, -1, -1, -1, -1, -1, -1, -1},
		{5, 10, 6, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{4, 3, 0, 4, 7, 3, 6, 5, 10, -1, -1, -1, -1, -1, -1, -1},
		{1, 9, 0, 5, 10, 6, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1},
		{10, 6, 5, 1, 9, 7, 1, 7, 3, 7, 9, 4, -1, -1, -1, -1},
		{6, 1, 2, 6, 5, 1, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1},
		{1, 2, 5, 5, 2, 6, 3, 0, 4, 3, 4, 7, -1, -1, -1, -1},
		{8, 4, 7, 9, 0, 5, 0, 6, 5, 0, 2, 6, -1, -1, -1, -1},
		{7, 3, 9, 7, 9, 4, 3, 2, 9, 5, 9, 6, 2, 6, 9, -1},
		{3, 11, 2, 7, 8, 4, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1},
		{5, 10, 6, 4, 7, 2, 4, 2, 0, 2, 7, 11, -1, -1, -1, -1},
		{0, 1, 9, 4, 7, 8, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1},
		{9, 2, 1, 9, 11, 2, 9, 4, 11, 7, 11, 4, 5, 10, 6, -1},
		{8, 4, 7, 3, 11, 5, 3, 5, 1, 5, 11, 6, -1, -1, -1, -1},
		{5, 1, 11, 5, 11, 6, 1, 0, 11, 7, 11, 4, 0, 4, 11, -1},
		{0, 5, 9, 0, 6, 5, 0, 3, 6, 11, 6, 3, 8, 4, 7, -1},
		{6, 5, 9, 6, 9, 11, 4, 7, 9, 7, 11, 9, -1, -1, -1, -1},
		{10, 4, 9, 6, 4, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{4, 10, 6, 4, 9, 10, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1},
		{10, 0, 1, 10, 6, 0, 6, 4, 0, -1, -1, -1, -1, -1, -1, -1},
		{8, 3, 1, 8, 1, 6, 8, 6, 4, 6, 1, 10, -1, -1, -1, -1},
		{1, 4, 9, 1, 2, 4, 2, 6, 4, -1, -1, -1, -1, -1, -1, -1},
		{3, 0, 8, 1, 2, 9, 2, 4, 9, 2, 6, 4, -1, -1, -1, -1},
		{0, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{8, 3, 2, 8, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1},
		{10, 4, 9, 10, 6, 4, 11, 2, 3, -1, -1, -1, -1, -1, -1, -1},
		{0, 8, 2, 2, 8, 11, 4, 9, 10, 4, 10, 6, -1, -1, -1, -1},
		{3, 11, 2, 0, 1, 6, 0, 6, 4, 6, 1, 10, -1, -1, -1, -1},
		{6, 4, 1, 6, 1, 10, 4, 8, 1, 2, 1, 11, 8, 11, 1, -1},
		{9, 6, 4, 9, 3, 6, 9, 1, 3, 11, 6, 3, -1, -1, -1, -1},
		{8, 11, 1, 8, 1, 0, 11, 6, 1, 9, 1, 4, 6, 4, 1, -1},
		{3, 11, 6, 3, 6, 0, 0, 6, 4, -1, -1, -1, -1, -1, -1, -1},
		{6, 4, 8, 11, 6, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{7, 10, 6, 7, 8, 10, 8, 9, 10, -1, -1, -1, -1, -1, -1, -1},
		{0, 7, 3, 0, 10, 7, 0, 9, 10, 6, 7, 10, -1, -1, -1, -1},
		{10, 6, 7, 1, 10, 7, 1, 7, 8, 1, 8, 0, -1, -1, -1, -1},
		{10, 6, 7, 10, 7, 1, 1, 7, 3, -1, -1, -1, -1, -1, -1, -1},
		{1, 2, 6, 1, 6, 8, 1, 8, 9, 8, 6, 7, -1, -1, -1, -1},
		{2, 6, 9, 2, 9, 1, 6, 7, 9, 0, 9, 3, 7, 3, 9, -1},
		{7, 8, 0, 7, 0, 6, 6, 0, 2, -1, -1, -1, -1, -1, -1, -1},
		{7, 3, 2, 6, 7, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{2, 3, 11, 10, 6, 8, 10, 8, 9, 8, 6, 7, -1, -1, -1, -1},
		{2, 0, 7, 2, 7, 11, 0, 9, 7, 6, 7, 10, 9, 10, 7, -1},
		{1, 8, 0, 1, 7, 8, 1, 10, 7, 6, 7, 10, 2, 3, 11, -1},
		{11, 2, 1, 11, 1, 7, 10, 6, 1, 6, 7, 1, -1, -1, -1, -1},
		{8, 9, 6, 8, 6, 7, 9, 1, 6, 11, 6, 3, 1, 3, 6, -1},
		{0, 9, 1, 11, 6, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{7, 8, 0, 7, 0, 6, 3, 11, 0, 11, 6, 0, -1, -1, -1, -1},
		{7, 11, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{3, 0, 8, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{0, 1, 9, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{8, 1, 9, 8, 3, 1, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1},
		{10, 1, 2, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{1, 2, 10, 3, 0, 8, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1},
		{2, 9, 0, 2, 10, 9, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1},
		{6, 11, 7, 2, 10, 3, 10, 8, 3, 10, 9, 8, -1, -1, -1, -1},
		{7, 2, 3, 6, 2, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{7, 0, 8, 7, 6, 0, 6, 2, 0, -1, -1, -1, -1, -1, -1, -1},
		{2, 7, 6, 2, 3, 7, 0, 1, 9, -1, -1, -1, -1, -1, -1, -1},
		{1, 6, 2, 1, 8, 6, 1, 9, 8, 8, 7, 6, -1, -1, -1, -1},
		{10, 7, 6, 10, 1, 7, 1, 3, 7, -1, -1, -1, -1, -1, -1, -1},
		{10, 7, 6, 1, 7, 10, 1, 8, 7, 1, 0, 8, -1, -1, -1, -1},
		{0, 3, 7, 0, 7, 10, 0, 10, 9, 6, 10, 7, -1, -1, -1, -1},
		{7, 6, 10, 7, 10, 8, 8, 10, 9, -1, -1, -1, -1, -1, -1, -1},
		{6, 8, 4, 11, 8, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{3, 6, 11, 3, 0, 6, 0, 4, 6, -1, -1, -1, -1, -1, -1, -1},
		{8, 6, 11, 8, 4, 6, 9, 0, 1, -1, -1, -1, -1, -1, -1, -1},
		{9, 4, 6, 9, 6, 3, 9, 3, 1, 11, 3, 6, -1, -1, -1, -1},
		{6, 8, 4, 6, 11, 8, 2, 10, 1, -1, -1, -1, -1, -1, -1, -1},
		{1, 2, 10, 3, 0, 11, 0, 6, 11, 0, 4, 6, -1, -1, -1, -1},
		{4, 11, 8, 4, 6, 11, 0, 2, 9, 2, 10, 9, -1, -1, -1, -1},
		{10, 9, 3, 10, 3, 2, 9, 4, 3, 11, 3, 6, 4, 6, 3, -1},
		{8, 2, 3, 8, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1},
		{0, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{1, 9, 0, 2, 3, 4, 2, 4, 6, 4, 3, 8, -1, -1, -1, -1},
		{1, 9, 4, 1, 4, 2, 2, 4, 6, -1, -1, -1, -1, -1, -1, -1},
		{8, 1, 3, 8, 6, 1, 8, 4, 6, 6, 10, 1, -1, -1, -1, -1},
		{10, 1, 0, 10, 0, 6, 6, 0, 4, -1, -1, -1, -1, -1, -1, -1},
		{4, 6, 3, 4, 3, 8, 6, 10, 3, 0, 3, 9, 10, 9, 3, -1},
		{10, 9, 4, 6, 10, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{4, 9, 5, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{0, 8, 3, 4, 9, 5, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1},
		{5, 0, 1, 5, 4, 0, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1},
		{11, 7, 6, 8, 3, 4, 3, 5, 4, 3, 1, 5, -1, -1, -1, -1},
		{9, 5, 4, 10, 1, 2, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1},
		{6, 11, 7, 1, 2, 10, 0, 8, 3, 4, 9, 5, -1, -1, -1, -1},
		{7, 6, 11, 5, 4, 10, 4, 2, 10, 4, 0, 2, -1, -1, -1, -1},
		{3, 4, 8, 3, 5, 4, 3, 2, 5, 10, 5, 2, 11, 7, 6, -1},
		{7, 2, 3, 7, 6, 2, 5, 4, 9, -1, -1, -1, -1, -1, -1, -1},
		{9, 5, 4, 0, 8, 6, 0, 6, 2, 6, 8, 7, -1, -1, -1, -1},
		{3, 6, 2, 3, 7, 6, 1, 5, 0, 5, 4, 0, -1, -1, -1, -1},
		{6, 2, 8, 6, 8, 7, 2, 1, 8, 4, 8, 5, 1, 5, 8, -1},
		{9, 5, 4, 10, 1, 6, 1, 7, 6, 1, 3, 7, -1, -1, -1, -1},
		{1, 6, 10, 1, 7, 6, 1, 0, 7, 8, 7, 0, 9, 5, 4, -1},
		{4, 0, 10, 4, 10, 5, 0, 3, 10, 6, 10, 7, 3, 7, 10, -1},
		{7, 6, 10, 7, 10, 8, 5, 4, 10, 4, 8, 10, -1, -1, -1, -1},
		{6, 9, 5, 6, 11, 9, 11, 8, 9, -1, -1, -1, -1, -1, -1, -1},
		{3, 6, 11, 0, 6, 3, 0, 5, 6, 0, 9, 5, -1, -1, -1, -1},
		{0, 11, 8, 0, 5, 11, 0, 1, 5, 5, 6, 11, -1, -1, -1, -1},
		{6, 11, 3, 6, 3, 5, 5, 3, 1, -1, -1, -1, -1, -1, -1, -1},
		{1, 2, 10, 9, 5, 11, 9, 11, 8, 11, 5, 6, -1, -1, -1, -1},
		{0, 11, 3, 0, 6, 11, 0, 9, 6, 5, 6, 9, 1, 2, 10, -1},
		{11, 8, 5, 11, 5, 6, 8, 0, 5, 10, 5, 2, 0, 2, 5, -1},
		{6, 11, 3, 6, 3, 5, 2, 10, 3, 10, 5, 3, -1, -1, -1, -1},
		{5, 8, 9, 5, 2, 8, 5, 6, 2, 3, 8, 2, -1, -1, -1, -1},
		{9, 5, 6, 9, 6, 0, 0, 6, 2, -1, -1, -1, -1, -1, -1, -1},
		{1, 5, 8, 1, 8, 0, 5, 6, 8, 3, 8, 2, 6, 2, 8, -1},
		{1, 5, 6, 2, 1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{1, 3, 6, 1, 6, 10, 3, 8, 6, 5, 6, 9, 8, 9, 6, -1},
		{10, 1, 0, 10, 0, 6, 9, 5, 0, 5, 6, 0, -1, -1, -1, -1},
		{0, 3, 8, 5, 6, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{10, 5, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{11, 5, 10, 7, 5, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{11, 5, 10, 11, 7, 5, 8, 3, 0, -1, -1, -1, -1, -1, -1, -1},
		{5, 11, 7, 5, 10, 11, 1, 9, 0, -1, -1, -1, -1, -1, -1, -1},
		{10, 7, 5, 10, 11, 7, 9, 8, 1, 8, 3, 1, -1, -1, -1, -1},
		{11, 1, 2, 11, 7, 1, 7, 5, 1, -1, -1, -1, -1, -1, -1, -1},
		{0, 8, 3, 1, 2, 7, 1, 7, 5, 7, 2, 11, -1, -1, -1, -1},
		{9, 7, 5, 9, 2, 7, 9, 0, 2, 2, 11, 7, -1, -1, -1, -1},
		{7, 5, 2, 7, 2, 11, 5, 9, 2, 3, 2, 8, 9, 8, 2, -1},
		{2, 5, 10, 2, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1},
		{8, 2, 0, 8, 5, 2, 8, 7, 5, 10, 2, 5, -1, -1, -1, -1},
		{9, 0, 1, 5, 10, 3, 5, 3, 7, 3, 10, 2, -1, -1, -1, -1},
		{9, 8, 2, 9, 2, 1, 8, 7, 2, 10, 2, 5, 7, 5, 2, -1},
		{1, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{0, 8, 7, 0, 7, 1, 1, 7, 5, -1, -1, -1, -1, -1, -1, -1},
		{9, 0, 3, 9, 3, 5, 5, 3, 7, -1, -1, -1, -1, -1, -1, -1},
		{9, 8, 7, 5, 9, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{5, 8, 4, 5, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1},
		{5, 0, 4, 5, 11, 0, 5, 10, 11, 11, 3, 0, -1, -1, -1, -1},
		{0, 1, 9, 8, 4, 10, 8, 10, 11, 10, 4, 5, -1, -1, -1, -1},
		{10, 11, 4, 10, 4, 5, 11, 3, 4, 9, 4, 1, 3, 1, 4, -1},
		{2, 5, 1, 2, 8, 5, 2, 11, 8, 4, 5, 8, -1, -1, -1, -1},
		{0, 4, 11, 0, 11, 3, 4, 5, 11, 2, 11, 1, 5, 1, 11, -1},
		{0, 2, 5, 0, 5, 9, 2, 11, 5, 4, 5, 8, 11, 8, 5, -1},
		{9, 4, 5, 2, 11, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{2, 5, 10, 3, 5, 2, 3, 4, 5, 3, 8, 4, -1, -1, -1, -1},
		{5, 10, 2, 5, 2, 4, 4, 2, 0, -1, -1, -1, -1, -1, -1, -1},
		{3, 10, 2, 3, 5, 10, 3, 8, 5, 4, 5, 8, 0, 1, 9, -1},
		{5, 10, 2, 5, 2, 4, 1, 9, 2, 9, 4, 2, -1, -1, -1, -1},
		{8, 4, 5, 8, 5, 3, 3, 5, 1, -1, -1, -1, -1, -1, -1, -1},
		{0, 4, 5, 1, 0, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{8, 4, 5, 8, 5, 3, 9, 0, 5, 0, 3, 5, -1, -1, -1, -1},
		{9, 4, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{4, 11, 7, 4, 9, 11, 9, 10, 11, -1, -1, -1, -1, -1, -1, -1},
		{0, 8, 3, 4, 9, 7, 9, 11, 7, 9, 10, 11, -1, -1, -1, -1},
		{1, 10, 11, 1, 11, 4, 1, 4, 0, 7, 4, 11, -1, -1, -1, -1},
		{3, 1, 4, 3, 4, 8, 1, 10, 4, 7, 4, 11, 10, 11, 4, -1},
		{4, 11, 7, 9, 11, 4, 9, 2, 11, 9, 1, 2, -1, -1, -1, -1},
		{9, 7, 4, 9, 11, 7, 9, 1, 11, 2, 11, 1, 0, 8, 3, -1},
		{11, 7, 4, 11, 4, 2, 2, 4, 0, -1, -1, -1, -1, -1, -1, -1},
		{11, 7, 4, 11, 4, 2, 8, 3, 4, 3, 2, 4, -1, -1, -1, -1},
		{2, 9, 10, 2, 7, 9, 2, 3, 7, 7, 4, 9, -1, -1, -1, -1},
		{9, 10, 7, 9, 7, 4, 10, 2, 7, 8, 7, 0, 2, 0, 7, -1},
		{3, 7, 10, 3, 10, 2, 7, 4, 10, 1, 10, 0, 4, 0, 10, -1},
		{1, 10, 2, 8, 7, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{4, 9, 1, 4, 1, 7, 7, 1, 3, -1, -1, -1, -1, -1, -1, -1},
		{4, 9, 1, 4, 1, 7, 0, 8, 1, 8, 7, 1, -1, -1, -1, -1},
		{4, 0, 3, 7, 4, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{4, 8, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{9, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{3, 0, 9, 3, 9, 11, 11, 9, 10, -1, -1, -1, -1, -1, -1, -1},
		{0, 1, 10, 0, 10, 8, 8, 10, 11, -1, -1, -1, -1, -1, -1, -1},
		{3, 1, 10, 11, 3, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{1, 2, 11, 1, 11, 9, 9, 11, 8, -1, -1, -1, -1, -1, -1, -1},
		{3, 0, 9, 3, 9, 11, 1, 2, 9, 2, 11, 9, -1, -1, -1, -1},
		{0, 2, 11, 8, 0, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{3, 2, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{2, 3, 8, 2, 8, 10, 10, 8, 9, -1, -1, -1, -1, -1, -1, -1},
		{9, 10, 2, 0, 9, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{2, 3, 8, 2, 8, 10, 0, 1, 8, 1, 10, 8, -1, -1, -1, -1},
		{1, 10, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{1, 3, 8, 9, 1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{0, 9, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{0, 3, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
		{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1} };

		int edgeTable[256] = {
		0x0  , 0x109, 0x203, 0x30a, 0x406, 0x50f, 0x605, 0x70c,
		0x80c, 0x905, 0xa0f, 0xb06, 0xc0a, 0xd03, 0xe09, 0xf00,
		0x190, 0x99 , 0x393, 0x29a, 0x596, 0x49f, 0x795, 0x69c,
		0x99c, 0x895, 0xb9f, 0xa96, 0xd9a, 0xc93, 0xf99, 0xe90,
		0x230, 0x339, 0x33 , 0x13a, 0x636, 0x73f, 0x435, 0x53c,
		0xa3c, 0xb35, 0x83f, 0x936, 0xe3a, 0xf33, 0xc39, 0xd30,
		0x3a0, 0x2a9, 0x1a3, 0xaa , 0x7a6, 0x6af, 0x5a5, 0x4ac,
		0xbac, 0xaa5, 0x9af, 0x8a6, 0xfaa, 0xea3, 0xda9, 0xca0,
		0x460, 0x569, 0x663, 0x76a, 0x66 , 0x16f, 0x265, 0x36c,
		0xc6c, 0xd65, 0xe6f, 0xf66, 0x86a, 0x963, 0xa69, 0xb60,
		0x5f0, 0x4f9, 0x7f3, 0x6fa, 0x1f6, 0xff , 0x3f5, 0x2fc,
		0xdfc, 0xcf5, 0xfff, 0xef6, 0x9fa, 0x8f3, 0xbf9, 0xaf0,
		0x650, 0x759, 0x453, 0x55a, 0x256, 0x35f, 0x55 , 0x15c,
		0xe5c, 0xf55, 0xc5f, 0xd56, 0xa5a, 0xb53, 0x859, 0x950,
		0x7c0, 0x6c9, 0x5c3, 0x4ca, 0x3c6, 0x2cf, 0x1c5, 0xcc ,
		0xfcc, 0xec5, 0xdcf, 0xcc6, 0xbca, 0xac3, 0x9c9, 0x8c0,
		0x8c0, 0x9c9, 0xac3, 0xbca, 0xcc6, 0xdcf, 0xec5, 0xfcc,
		0xcc , 0x1c5, 0x2cf, 0x3c6, 0x4ca, 0x5c3, 0x6c9, 0x7c0,
		0x950, 0x859, 0xb53, 0xa5a, 0xd56, 0xc5f, 0xf55, 0xe5c,
		0x15c, 0x55 , 0x35f, 0x256, 0x55a, 0x453, 0x759, 0x650,
		0xaf0, 0xbf9, 0x8f3, 0x9fa, 0xef6, 0xfff, 0xcf5, 0xdfc,
		0x2fc, 0x3f5, 0xff , 0x1f6, 0x6fa, 0x7f3, 0x4f9, 0x5f0,
		0xb60, 0xa69, 0x963, 0x86a, 0xf66, 0xe6f, 0xd65, 0xc6c,
		0x36c, 0x265, 0x16f, 0x66 , 0x76a, 0x663, 0x569, 0x460,
		0xca0, 0xda9, 0xea3, 0xfaa, 0x8a6, 0x9af, 0xaa5, 0xbac,
		0x4ac, 0x5a5, 0x6af, 0x7a6, 0xaa , 0x1a3, 0x2a9, 0x3a0,
		0xd30, 0xc39, 0xf33, 0xe3a, 0x936, 0x83f, 0xb35, 0xa3c,
		0x53c, 0x435, 0x73f, 0x636, 0x13a, 0x33 , 0x339, 0x230,
		0xe90, 0xf99, 0xc93, 0xd9a, 0xa96, 0xb9f, 0x895, 0x99c,
		0x69c, 0x795, 0x49f, 0x596, 0x29a, 0x393, 0x99 , 0x190,
		0xf00, 0xe09, 0xd03, 0xc0a, 0xb06, 0xa0f, 0x905, 0x80c,
		0x70c, 0x605, 0x50f, 0x406, 0x30a, 0x203, 0x109, 0x0 };

		static Vec3 marchVertices[] = {
			Vec3(0.5, 0, 0),
			Vec3(1	, 0, 0.5),
			Vec3(0.5, 0, 1),
			Vec3(0	, 0, 0.5),

			Vec3(0.5, 1, 0),
			Vec3(1	, 1, 0.5),
			Vec3(0.5, 1, 1),
			Vec3(0	, 1, 0.5),

			Vec3(0	, 0.5, 0),
			Vec3(1	, 0.5, 0),
			Vec3(1	, 0.5, 1),
			Vec3(0	, 0.5, 1)
		};

		static Vec3 interp(Vec3 a, Vec3 b, float wb, float wa, float cutOff) {
			
			float w = wb - wa;
			if (fabs(w) < 0.01)w = 0.5;
			w += 0.5;
			//w += cutOff;
			Vec3 d = b - a;
			if (fabs(w) < 0.01)return a;
			if (fabs(w-1) < 0.01)return b;

			//if (w < 0)w = 0;
			//if (w > 1)w = 1;
			//std::cout << std::to_string(w) << std::endl;
			w = 0.5;
			//return a + (d * w);
			return (a + b) / 2;
		}

		/*
		mpVector LinearInterp(mp4Vector p1, mp4Vector p2, float value)
{
    if (p2 < p1)
    {
        mp4Vector temp;
        temp = p1;
        p1 = p2;
        p2 = temp;    
    }

    mpVector p;
    if(fabs(p1.val - p2.val) > 0.00001)
        p = (mpVector)p1 + ((mpVector)p2 - (mpVector)p1)/(p2.val - p1.val)*(value - p1.val);
    else 
        p = (mpVector)p1;
    return p;
}

    bool operator<(const mp4Vector &right) const
    {
        if (x < right.x)
            return true;
        else if (x > right.x)
            return false;

        if (y < right.y)
            return true;
        else if (y > right.y)
            return false;

        if (z < right.z)
            return true;
        else if (z > right.z)
            return false;

        return false;
     }
*/

		Mesh MeshTools::Volume(Vec3 size, NoiseFunction3D* noise, Texture* texture) {
			std::vector<Texture*> textures;
			std::vector<Vertex3D> vertices;
			std::vector<GLuint> indices;
			float isoCutOff = 0.5;
			std::map<Vec3, int> indexMap;
			int index = 0;

			for (int i = 0; i < size.x; i++) {
				for (int j = 0; j < size.y; j++) {
					for (int k = 0; k < size.z; k++) {
						Vec3 pos = Vec3(i, j, k);
						int cubeIndex = 0;

						Vec3 grid[] = {
							Vec3(0, 0, 0),//0
							Vec3(1, 0, 0),//1
							Vec3(1, 0, 1),//2
							Vec3(0, 0, 1),//3
							Vec3(0, 1, 0),//4
							Vec3(1, 1, 0),//5
							Vec3(1, 1, 1),//6
							Vec3(0, 1, 1)//7
						};
						float noiseVals[8];
						Vec3 vertList[12]{
							Vec3(0, 0, 0),
							Vec3(0, 0, 0),
							Vec3(0, 0, 0),
							Vec3(0, 0, 0),
							Vec3(0, 0, 0),
							Vec3(0, 0, 0),
							Vec3(0, 0, 0),
							Vec3(0, 0, 0),
							Vec3(0, 0, 0),
							Vec3(0, 0, 0),
							Vec3(0, 0, 0),
							Vec3(0, 0, 0)
						};

						for (int a = 0; a < 8; a++) {
							noiseVals[a] = noise->noise(grid[7-a].x + i, grid[7-a].y + j, grid[7-a].z + k);

							if (noiseVals[a] < isoCutOff) cubeIndex++;
							cubeIndex <<= 1;
						}
						cubeIndex >>= 1;
						//std::cout << std::to_string(cubeIndex) << std::endl;
						
						if (edgeTable[cubeIndex] & 1 | true) vertList[0] = interp(grid[0], grid[1], noiseVals[1], noiseVals[0], isoCutOff);
						if (edgeTable[cubeIndex] & 2 | true) vertList[1] = interp(grid[1], grid[2], noiseVals[2], noiseVals[1], isoCutOff);
						if (edgeTable[cubeIndex] & 4 | true) vertList[2] = interp(grid[2], grid[3], noiseVals[3], noiseVals[2], isoCutOff);
						if (edgeTable[cubeIndex] & 8 | true) vertList[3] = interp(grid[3], grid[0], noiseVals[0], noiseVals[3], isoCutOff);

						if (edgeTable[cubeIndex] & 16 | true) vertList[4] = interp(grid[4], grid[5], noiseVals[5], noiseVals[4], isoCutOff);
						if (edgeTable[cubeIndex] & 32 | true) vertList[5] = interp(grid[5], grid[6], noiseVals[6], noiseVals[5], isoCutOff);
						if (edgeTable[cubeIndex] & 64 | true) vertList[6] = interp(grid[6], grid[7], noiseVals[7], noiseVals[6], isoCutOff);
						if (edgeTable[cubeIndex] & 128 | true) vertList[7] = interp(grid[7], grid[4], noiseVals[4], noiseVals[7], isoCutOff);
						
						if (edgeTable[cubeIndex] & 256 | true) vertList[8] = interp(grid[0], grid[4], noiseVals[4], noiseVals[0], isoCutOff);
						if (edgeTable[cubeIndex] & 512 | true)	vertList[9] = interp(grid[1], grid[5], noiseVals[5], noiseVals[1], isoCutOff);
						if (edgeTable[cubeIndex] & 1024 | true) vertList[10] = interp(grid[2], grid[6], noiseVals[6], noiseVals[2], isoCutOff);
						if (edgeTable[cubeIndex] & 2048 | true) vertList[11] = interp(grid[3], grid[7], noiseVals[7], noiseVals[3], isoCutOff);
						

						for (int a = 0; a < 15; a++) {
							//std::cout << std::to_string(cubeIndex) << " : " << std::to_string(a) << std::endl;
							int vertexIndex = triTable[cubeIndex][a];
							//faces[cubeIndex * 15 + a];
							if (a != -1) {

								//Vec3 vertexPos = vertList[triTable[cubeIndex][a]];
								Vec3 vertexPos = marchVertices[vertexIndex] + pos;
								//Vec3 vertexPos = vertList[vertexIndex] + pos;
								//vertexPos.z = 1 - vertexPos.z;
								//vertexPos.add(pos);
								if (indexMap.find(vertexPos) == indexMap.end()) {
									Vertex3D vertex = Vertex3D(vertexPos, Vec3(0, 0, 0), Vec2(0, j / size.y));
									vertices.push_back(vertex);

									indexMap[vertexPos] = index;
									index++;
								}
								indices.push_back(indexMap[vertexPos]);



							}
						}
					}
				}
			}
			/*
			std::cout << "Amount of vertices :" << std::to_string(vertices.size()) << std::endl;
			for (int i = 0; i < vertices.size(); i++) {
				indices.push_back(i);
			}
			*/






			textures.push_back(texture);

			Mesh result = Mesh(vertices, indices, textures);
			result.recalculateNormals();
			return result;
		}

	}
}