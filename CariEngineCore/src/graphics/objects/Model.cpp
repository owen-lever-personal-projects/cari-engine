#include "Model.h"

#include <ASSIMP/Importer.hpp>
#include <ASSIMP/postprocess.h>
#include "../textures/TextureManager.h"

namespace CariEngine { namespace Graphics {

	Model::Model(const char* path, Shader* shader) {
		this->shader = shader;
		std::cout << path << std::endl;
		loadModel(path);
	}

	Model::Model(Mesh mesh, Shader* shader) {
		this->shader = shader;
		mesh.setupMesh(shader);
		meshes.push_back(mesh);
		directory = "MeshTools";

	}


	void Model::draw() {
		for (int i = 0; i < meshes.size(); i++) {
			meshes[i].draw(shader);
		}
	}

	void Model::loadModel(const char* path) {
		Assimp::Importer importer;
		const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate /* | aiProcess_FlipUVs*/);
		
		if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
		{
			std::cout << "ERROR::ASSIMP::" << importer.GetErrorString() << std::endl;
			return;
		}
		directory = path;
		directory = directory.substr(0, directory.find_last_of('/')+1);

		processNode(scene->mRootNode, scene);


	}

	void Model::processNode(aiNode* node, const aiScene* scene) {

		for (int i = 0; i < node->mNumMeshes; i++) {
			aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
			meshes.push_back(processMesh(mesh, scene));
		}
		for (int i = 0; i < node->mNumChildren; i++) {
			processNode(node->mChildren[i], scene);
		}

	}

	Mesh Model::processMesh(aiMesh* mesh, const aiScene* scene) {
		
		std::vector<Vertex3D> vertices;
		std::vector<GLuint> indices;
		std::vector<Texture*> textures;

		for (int i = 0; i < mesh->mNumVertices; i++) {
			Vertex3D vertex;

			vertex.position.x = mesh->mVertices[i].x;
			vertex.position.y = mesh->mVertices[i].y;
			vertex.position.z = mesh->mVertices[i].z;

			if (mesh->HasNormals()) {
				vertex.normal.x = mesh->mNormals[i].x;
				vertex.normal.y = mesh->mNormals[i].y;
				vertex.normal.z = mesh->mNormals[i].z;
			}
			
			if (mesh->mTextureCoords[0]) {
				vertex.uv.x = mesh->mTextureCoords[0][i].x;
				vertex.uv.y = mesh->mTextureCoords[0][i].y;
			}
			/*
			if (mesh->HasVertexColors()) {
				vertex.color.x = mesh->mColors[i]->r;
				vertex.color.y = mesh->mColors[i]->g;
				vertex.color.z = mesh->mColors[i]->b;
				vertex.color.w = mesh->mColors[i]->a;
			}
			*/

			vertex.color = Vec4(1, 1, 1, 1);

			vertices.push_back(vertex);
		}

		for (int i = 0; i < mesh->mNumFaces; i++) {
			
			aiFace face = mesh->mFaces[i];
			for (int j = 0; j < face.mNumIndices; j++) {
				indices.push_back(face.mIndices[j]);
			}
			//indices.push_back(mesh->mFaces[i].mIndices[0]);
			//indices.push_back(mesh->mFaces[i].mIndices[1]);
			//indices.push_back(mesh->mFaces[i].mIndices[2]);
		}

		
		if (mesh->mMaterialIndex >= 0) {
			aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
			std::vector<Texture*> diffuseMaps = loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");

			textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
			std::vector<Texture*> specularMaps = loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
			
			textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
		}
		


		Mesh modelMesh = Mesh(vertices, indices, textures);
		modelMesh.setupMesh(shader);
		return modelMesh;
	}

	
	std::vector<Texture*> Model::loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName)
	{
		std::vector<Texture*> textures;
		for (unsigned int i = 0; i < mat->GetTextureCount(type); i++)
		{
			aiString str;
			mat->GetTexture(type, i, &str);
			std::cout << str.C_Str() << std::endl;
			//std::cout << "dir : " << directory << str.C_Str() << std::endl;
		
			std::string path = directory + str.C_Str();
			Texture* texture = TextureManager::getTexture(path);

			//texture.id = TextureFromFile(str.C_Str(), directory);
			//texture.type = typeName;
			//texture.path = str;
			
			textures.push_back(texture);
		}
		return textures;
	}












}}