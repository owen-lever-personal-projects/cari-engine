#include "ModelManager.h"
#include "../shaders/Shader.h"
#include <vector>
#include <list>

namespace CariEngine {
	namespace Graphics {

		std::map<std::string, Model> ModelManager::models;
		Shader* ModelManager::shader = nullptr;
		std::vector<Model> meshModels = std::vector<Model>(); 

		Model* ModelManager::getModel(std::string filePath) {
			if (models.count(filePath) == 0) {
				Model model = Model(filePath.c_str(), shader);
				models[filePath] = model;
			}
			return &models[filePath];
		}

		void ModelManager::init(Shader* shader) {
			ModelManager::shader = shader;
			meshModels.reserve(1000);
		}

		Model* ModelManager::getModel(Mesh mesh) {
			meshModels.push_back(Model(mesh, shader));
			return &meshModels[meshModels.size()-1];
		}



	}
}