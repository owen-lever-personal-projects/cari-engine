#include "Font.h"

using namespace CariEngine;
using namespace Graphics;
using namespace std;

int Font::getWidth(string text) {
	int width = 0;

	for (char c : text) {
		FontCharacter fc;
		if (characters.find(c) == characters.end()) {
			fc = characters[' '];
		}
		else {
			fc = characters[c];
		}
		width += fc.xAdvance;
	}

	return width;
}