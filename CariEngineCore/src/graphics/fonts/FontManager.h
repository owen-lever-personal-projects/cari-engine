#pragma once

#include <list>
#include <string>
#include "Font.h"

namespace CariEngine { namespace Graphics {
	using namespace std;

	class FontManager {
	public:
		static void Init();
		//static Font* loadFont(string filePath, int size = defaultFontSize);
		static Font* loadBitmapFont(string filePath);
		//static Font* getFontByName(string name);
		static void unloadFont(Font* font);

		static int defaultFontSize;
	private:
		static bool init;
		static std::list<Font> fonts;


	};



}}