#pragma once

#include <string>
#include "../textures/Texture.h"
#include "../../maths/maths.h"
#include <map>

namespace CariEngine { namespace Graphics {
	using namespace Maths;
	using namespace std;

	struct FontCharacter {
		int id;
		Vec2 position;
		Vec2 size;
		Vec2 offset;
		float xAdvance;
		int page;
	};

	struct FontPage {
		int id;
		Texture* texture;
	};

	struct FontKerning {
		int first;
		int second;
		int amount;
	};

	struct Font {

		int getWidth(string text);

		std::string name; 
		float size;
		bool bold;
		bool italic;
		string charset;
		bool unicode;
		int stretchH;
		bool smooth;
		bool aa;
		int paddingL, paddingR, paddingT, paddingB;
		int spacingL, spacingR;

		int lineHeight;
		int fontBase;
		int scaleW;
		int scaleH;
		bool packed;

		std::map<int, FontPage> pages;
		std::map<char, FontCharacter> characters;




	};







}}