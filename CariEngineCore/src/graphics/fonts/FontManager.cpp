#include "FontManager.h"
#include "../../utils/FileReader.h"
#include "../../utils/StringUtils.h"
#include "../../graphics/textures/Texture.h"
#include "../../graphics/textures/TextureManager.h"

using namespace CariEngine;
using namespace Graphics;
using namespace std;

std::list<Font >FontManager::fonts = std::list<Font>();

void FontManager::Init() {
	


}

void FontManager::unloadFont(Font* font) {
	std::list<Font>::iterator it;
	for (it = fonts.begin(); it != fonts.end(); it++) {
		if (font == (&*it)) {
			fonts.erase(it);
			break;
		}
	}
}

Font* FontManager::loadBitmapFont(string filePath) {
	int dirOffset = filePath.find_last_of("/");
	string dir = filePath.substr(0, dirOffset+1);
	fonts.push_back(Font());
	Font* font = &fonts.back();
	vector<string> lines = split_string(read_file(filePath.c_str()), "\n");

	for (string l : lines) {
		if (l.length() == 0)
			continue;
		string type = split_string(l, " ")[0];
		vector<string> data = split_string(l.substr(type.length() + 1), " ");
		map<string, string> values;
		for (string d : data) {
			if (d.length() > 0) {
				vector<string> valuePair = split_string(d, "=");
				if (valuePair.size() == 2) {
					string k = valuePair[0];
					string v = valuePair[1];
					if (v.front() == '"' && v.back() == '"') {
						v = v.substr(1, v.length() - 2);
					}
					values[k] = v;
				}
			}
		}

		if (type == "info") {
			if (values.find("face") != values.end())
				font->name = values["face"];

			if (values.find("size") != values.end())
				font->size = atoi(values["size"].c_str());

			if (values.find("bold") != values.end())
				font->bold = atoi(values["bold"].c_str());

			if (values.find("italic") != values.end())
				font->italic = atoi(values["italic"].c_str());

			if (values.find("charset") != values.end())
				font->charset = values["charset"];

			if (values.find("unicode") != values.end())
				font->unicode = atoi(values["unicode"].c_str());

			if (values.find("stretchH") != values.end())
				font->stretchH = atoi(values["stretchH"].c_str());

			if (values.find("smooth") != values.end())
				font->smooth = atoi(values["smooth"].c_str());

			if (values.find("aa") != values.end())
				font->aa = atoi(values["aa"].c_str());

			if (values.find("padding") != values.end()) {
				vector<string> paddingValues = split_string(values["padding"], ",");
				if (paddingValues.size() == 4) {
					font->paddingL = atoi(paddingValues[0].c_str());
					font->paddingR = atoi(paddingValues[1].c_str());
					font->paddingT = atoi(paddingValues[2].c_str());
					font->paddingB = atoi(paddingValues[3].c_str());
				}
			}

			if (values.find("spacing") != values.end()) {
				vector<string> spacingValues = split_string(values["spacing"], ",");
				if (spacingValues.size() == 2) {
					font->spacingL = atoi(spacingValues[0].c_str());
					font->spacingR = atoi(spacingValues[1].c_str());
				}
			}
		}

		else if (type == "common") {
			if (values.find("lineHeight") != values.end())
				font->lineHeight = atoi(values["lineHeight"].c_str());

			if (values.find("base") != values.end())
				//font->base = atoi(values["base"].c_str());

			if (values.find("scaleW") != values.end())
				font->scaleW = atoi(values["scaleW"].c_str());

			if (values.find("scaleH") != values.end())
				font->scaleH = atoi(values["scaleH"].c_str());

			if (values.find("packed") != values.end())
				font->packed = atoi(values["packed"].c_str());
		}

		else if (type == "page") {
			if (values.find("id") != values.end() && values.find("file") != values.end()) {
				FontPage page;
				page.texture = TextureManager::getTexture(dir + values["file"], true);
				page.id = atoi(values["id"].c_str());
				font->pages[page.id] = page;
			}
		}

		else if (type == "char") {
			FontCharacter character;

			if (values.find("id") != values.end())
				character.id = atoi(values["id"].c_str());

			if (values.find("x") != values.end())
				character.position.x = atoi(values["x"].c_str());

			if (values.find("y") != values.end())
				character.position.y = atoi(values["y"].c_str());

			if (values.find("width") != values.end())
				character.size.x = atoi(values["width"].c_str());

			if (values.find("height") != values.end())
				character.size.y = atoi(values["height"].c_str());

			if (values.find("xoffset") != values.end())
				character.offset.x = atoi(values["xoffset"].c_str());

			if (values.find("yoffset") != values.end())
				character.offset.y = atoi(values["yoffset"].c_str());

			if (values.find("xadvance") != values.end())
				character.xAdvance = atoi(values["xadvance"].c_str());
			
			if (values.find("page") != values.end()) {
				character.page = atoi(values["page"].c_str());
			}

			font->characters[character.id] = character;
		}
	}
	/*
	for (std::map<char, FontCharacter>::iterator it = font->characters.begin(); it != font->characters.end(); it++) {
		FontCharacter* fc = &font->characters[it->first];
		FontPage* page = &font->pages[fc->page];
		Vec2 texSize = Vec2(page->texture.width, page->texture.height);
		fc->position = fc->position / texSize;
		fc->size = fc->size / texSize;
	}
	*/

	return font;
}

/*
Vec2 texSize = Vec2(font->pages[0].texture.width, font->pages[0].texture.height);

						Vec2 uvPos = fc->position / texSize;
						//uvPos.y = 1 - uvPos.y;
						Vec2 uvSize = fc->size / texSize;
*/