#pragma once

#include <GL/glew.h>


namespace CariEngine { namespace Graphics {
	class Buffer {
	public:
		Buffer(GLfloat* data, GLsizei count, GLuint componentCount);
		~Buffer();
		void bind() const;
		void unbind() const;
		GLuint getComponentCount() const;

	private:
		GLuint bufferId;
		GLuint componentCount;
	};

}}