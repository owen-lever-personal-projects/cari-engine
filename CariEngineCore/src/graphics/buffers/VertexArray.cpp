#include "VertexArray.h"

namespace CariEngine { namespace Graphics {
	
	VertexArray::VertexArray() {
		glGenVertexArrays(1, &arrayId);
	}


	VertexArray::~VertexArray() {
		for (int i = 0; i < buffers.size(); i++) {
			delete buffers[i];
		}
		glDeleteVertexArrays(1, &arrayId);
	}

	void VertexArray::addBuffer(Buffer* buffer, GLuint index) {
		bind();
		buffer->bind();
		glEnableVertexAttribArray(index);
		glVertexAttribPointer(index, buffer->getComponentCount(), GL_FLOAT, GL_FALSE, 0, 0);


		buffer->unbind();
		unbind();

		buffers.push_back(buffer);
	}

	void VertexArray::bind() const {
		glBindVertexArray(arrayId);
	}
	void VertexArray::unbind() const {
		glBindVertexArray(0);
	}
}}