#pragma once
#include <GL/glew.h>
#include <vector>
#include "Buffer.h"
namespace CariEngine { namespace Graphics {
	class VertexArray {
		public:
			VertexArray();
			~VertexArray();
			void addBuffer(Buffer* buffer, GLuint index);
			void bind() const;
			void unbind() const;

		private:
			GLuint arrayId;
			std::vector<Buffer*> buffers;

	};
}}