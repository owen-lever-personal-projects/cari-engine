#include "IndexBuffer.h"



namespace CariEngine { namespace Graphics {

	IndexBuffer::IndexBuffer(GLushort* data, GLsizei count) {
		this->count = count;
		glGenBuffers(1, &bufferId);
		bind();
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(GLushort), data, GL_STATIC_DRAW);
		unbind();

	}

	void IndexBuffer::bind() const {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferId);
	}

	void IndexBuffer::unbind() const {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	GLuint IndexBuffer::getCount() const {
		return count;
	}

	IndexBuffer::~IndexBuffer() {
		glDeleteBuffers(1, &bufferId);
	}



}}