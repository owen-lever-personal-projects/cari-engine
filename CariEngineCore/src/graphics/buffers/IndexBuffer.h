#pragma once

#include <GL/glew.h>


namespace CariEngine { namespace Graphics {
	class IndexBuffer {
	public:
		IndexBuffer(GLushort* data, GLsizei count);
		~IndexBuffer();
		void bind() const;
		void unbind() const;
		GLuint getCount() const;

	private:
		GLuint bufferId;
		GLuint count;
	};

}}