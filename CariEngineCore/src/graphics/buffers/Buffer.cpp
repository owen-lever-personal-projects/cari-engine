#include "Buffer.h"
#include <iostream>


namespace CariEngine { namespace Graphics {
	
	Buffer::Buffer(GLfloat* data, GLsizei count, GLuint componentCount) {
		this->componentCount = componentCount;
		glGenBuffers(1, &bufferId);
		bind();
		glBufferData(GL_ARRAY_BUFFER, count * sizeof(GLfloat), data, GL_STATIC_DRAW);
		unbind();

	}

	void Buffer::bind() const {
		glBindBuffer(GL_ARRAY_BUFFER, bufferId);
	}

	void Buffer::unbind() const {
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	GLuint Buffer::getComponentCount() const {
		return componentCount;
	}

	Buffer::~Buffer() {
		glDeleteBuffers(1, &bufferId);
	}





}}