#pragma once
#include "../buffers/Buffer.h"
#include "../buffers/IndexBuffer.h"
#include "../buffers/VertexArray.h"
#include "../../maths/maths.h"

namespace CariEngine { namespace Graphics {
	using namespace Maths;



	struct RenderableGuiElement {

		public:

			RenderableGuiElement(Vec2 position, Vec2 size, Vec4 color, Vec2 uvPos = Vec2(0, 0), Vec2 uvSize = Vec2(1, 1))
			{
				this->position = position;
				this->size = size;
				this->color = color;
				this->rotation = 0;
				this->uvPos = uvPos;
				this->uvSize = uvSize;
			}

			~RenderableGuiElement() {
				
			}

			Vec2 position;
			Vec2 size;
			Vec2 uvPos;
			Vec2 uvSize;
			Vec4 color;
			float rotation;
		private:

		protected:

			



	};

}}