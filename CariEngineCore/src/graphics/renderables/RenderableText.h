#pragma once
#include "../../maths/maths.h"
#include <string>
#include "../fonts/Font.h"

namespace CariEngine { namespace Graphics{
	using namespace Maths;
	using namespace std;

	class RenderableText {
	public:
		RenderableText(string text, Font* font, Vec2 position = Vec2(0, 0), Vec4 color = Vec4(1, 1, 1, 1))
		{
			this->text = text;
			this->font = font;
			this->position = position;
			this->color = color;
		}


		~RenderableText() {

		}

		Font* font;
		string text;
		Vec4 color;

		Vec2 position;
		float size;


	private:

	protected:


	};




}}