#pragma once
#include "../../maths/maths.h"
#include "../textures/Texture.h"

namespace CariEngine { namespace Graphics {
	using namespace Maths;



	class Renderable2D {

		public:
			inline const Vec3& getPosition() const {
				return position;
			}
			inline const Vec2& getSize() const {
				return size;
			}
			inline const Vec4& getColor() const {
				return color;
			}
			inline float getRotation() const {
				return rotation;
			}
			void setColor(Vec4 color) {
				this->color = color;
			}
			void setPosition(Vec3 position) {
				this->position = position;
			}
			void setSize(Vec2 size) {
				this->size = size;
			}
			void addRotation(float rotation) {
				this->rotation += rotation;
			}
			void setRotation(float rotation) {
				this->rotation = rotation;
			}
			Texture* getTexture() {
				return texture;
			}

			Renderable2D(Vec3 position, Vec2 size, Vec4 color, Texture* texture)
			{
				this->position = position;
				this->size = size;
				this->color = color;
				this->rotation = 0;
				this->texture = texture;

			}

			~Renderable2D() {
				
			}
		private:

		protected:
			Vec3 position;
			Vec2 size;
			Vec4 color;
			float rotation;
			Texture* texture;
			



	};

}}