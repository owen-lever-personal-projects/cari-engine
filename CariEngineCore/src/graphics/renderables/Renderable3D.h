#pragma once

#include "../objects/Model.h"
#include "../../maths/maths.h"
#include "../buffers/VertexArray.h"
#include "../buffers/IndexBuffer.h"
#include <GL/glew.h>
#include "../../maths/maths.h"

namespace CariEngine { namespace Graphics {
		using namespace Maths;
		class Renderable3D {
		public:
			Renderable3D(Vec3 position, Model* model) : roll(0), pitch(0), yaw(0)
			{
				this->position = position;
				this->model = model;
				this->color = Vec4(1, 1, 1, 1);
				this->scale = Vec3(1, 1, 1);
			}

			void draw() const{
				model->draw();
			}

			~Renderable3D() {

			}


			Vec3 position;
			Vec3 scale;


			float pitch;
			float yaw;
			float roll;


			Model* model;
		private:

		protected:
			//Vec2 size;
			Vec4 color;


		};

}}