#pragma once

#include <GL/glew.h>
#include <string>
#include "Texture.h"
#include "../../utils/Image.h"

namespace CariEngine { namespace Graphics {
	
	class CubeMap {
	public:
		GLuint id;

		CubeMap() : id(0){

		};

		CubeMap(std::string faces[6]) {
			glGenTextures(1, &id);
			bind();

			for (int i = 0; i < 6; i++) {
				Image image = Image(faces[i], true);

				std::cout << faces[i] << " " << std::to_string(image.width) << " " << std::to_string(image.height) << std::endl;
				glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, image.width, image.height, 0, GL_BGRA, GL_UNSIGNED_BYTE, image.data);

			}

			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

			unbind();
		}

		void bind() {
			glBindTexture(GL_TEXTURE_CUBE_MAP, id);
		}
		void unbind() {
			glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
		}


	};





}}