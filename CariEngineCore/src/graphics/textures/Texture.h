#pragma once

#include <FI/FreeImage.h>
#include <string>
#include <GL/glew.h>

namespace CariEngine { namespace Graphics {
	
	struct Texture {
			int width;
			int height;
			GLuint id;

		public:
			//Type type;
			Texture(std::string filePath, bool vFlipped);
			Texture(int width, int height, BYTE* data);
			Texture();
			~Texture();
			void bind();
			void unbind();
			static void loadTexture(std::string filePath, int& width , int& height, BYTE*& bits);

	};


	enum Type {
		Normal,
		SpecTexture,
		DiffuseTexture
	};



}}