#include "Texture.h"

#include <FI/FreeImage.h>
#include <iostream>
#include "../../maths/maths.h"
#include <vector>
#include "../../utils/Image.h"
using namespace CariEngine;
using namespace Graphics;
using namespace Maths;

void Texture::bind() {
	glBindTexture(GL_TEXTURE_2D, id);
}

void Texture::unbind() {
	glBindTexture(GL_TEXTURE_2D, 0);
}

Texture::~Texture() {
}


Texture::Texture() {

}

Texture::Texture(int width, int height, BYTE* data) {
	glGenTextures(1, &id);
	bind();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &data);

	unbind();
}


void Texture::loadTexture(std::string filePath, int& width, int& height, BYTE*& bits) {
	std::cout << "Texture loaded : " << filePath << std::endl;
	//image format
	FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
	//pointer to the image, once loaded
	FIBITMAP* dib(0);

	//check the file signature and deduce its format
	fif = FreeImage_GetFileType(filePath.c_str(), 0);
	//if still unknown, try to guess the file format from the file extension
	if (fif == FIF_UNKNOWN)
		fif = FreeImage_GetFIFFromFilename(filePath.c_str());
	//if still unkown, return failure
	if (fif == FIF_UNKNOWN) {
		std::cout << "Unknown file format!" << std::endl;
		return;

	}

	//check that the plugin has reading capabilities and load the file
	if (FreeImage_FIFSupportsReading(fif))
		dib = FreeImage_Load(fif, filePath.c_str());
	//if the image failed to load, return failure
	if (!dib) {
		std::cout << "Image Failed to load!" << std::endl;
		return;
	}

	dib = FreeImage_ConvertTo32Bits(dib);

	//retrieve the image data
	bits = FreeImage_GetBits(dib);
	//get the image width and height
	width = FreeImage_GetWidth(dib);
	height = FreeImage_GetHeight(dib);

	FreeImage_Unload(dib);
}

Texture::Texture(std::string filePath, bool vFlipped){

	//BYTE* bits(0);
	//std::vector<BYTE> bits;
	Image image = Image(filePath, vFlipped);
	width = image.width;
	height = image.height;
	//loadTexture(filePath, width, height, bits);


	glGenTextures(1, &id);
	bind();

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, image.data);

	unbind();
	
}