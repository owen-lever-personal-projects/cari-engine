#include "TextureManager.h"

namespace CariEngine { namespace Graphics {

	std::map<std::string, Texture> TextureManager::textures;
	std::string TextureManager::textureDir;
	Texture TextureManager::error = Texture();
	
	Texture* TextureManager::getTexture(std::string filePath, bool vFlipped) {
		filePath = textureDir +filePath;
		if (textures.count(filePath) == 0) {
			Texture texture = Texture(filePath, vFlipped);
			textures[filePath] = texture;
		}
		return &textures[filePath];
		
	}

	Texture* TextureManager::getError() {
		if (error.id == 0) {
			BYTE colors[256];

			for (int i = 0; i < 8; i++) {
				for (int j = 0; j < 8; j++) {
					if ((i + j) % 2 == 0) {
						colors[j * 32 + i * 4 + 0] = 0;
						colors[j * 32 + i * 4 + 1] = 0;
						colors[j * 32 + i * 4 + 2] = 0;
						colors[j * 32 + i * 4 + 3] = 255;
					}
					else {
						colors[j * 32 + i * 4 + 0] = 255;
						colors[j * 32 + i * 4 + 1] = 0;
						colors[j * 32 + i * 4 + 2] = 255;
						colors[j * 32 + i * 4 + 3] = 255;
					}
				}
			}
			error = Texture(8, 8, &colors[0]);
		}
		return &TextureManager::error;
	}

}}

/*
namespace CariEngine{ namespace Graphics{

	class TextureManager {
		static std::map<std::string, Texture*> textures;
		static Texture error;

	public:
		static std::string textureDir;

		static Texture* getTexture(std::string filePath, bool vFlipped = false);

		static Texture* getError();

	};



}}
*/