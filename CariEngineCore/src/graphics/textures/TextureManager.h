#pragma once
#include <map>
#include <string>
#include "Texture.h"
#include <memory>

namespace CariEngine{ namespace Graphics{

	class TextureManager {
		static std::map<std::string, Texture> textures;
		static Texture error;

	public:
		static std::string textureDir;

		static Texture* getTexture(std::string filePath, bool vFlipped = false);

		static Texture* getError();

	};



}}