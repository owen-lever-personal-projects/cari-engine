#include "Shader.h"

#include <vector>
#include <iostream>
#include "../../utils/FileReader.h"
#include <string>

using namespace CariEngine;
using namespace Maths;

Shader::Shader() : numAttributes(0), programId(0), vertId(0), fragId(0)
{

}

Shader::~Shader()
{

}

void Shader::compileShaders(const std::string& vertexShaderFilePath, const std::string& fragmentShaderFilePath)
{
	std::cout << "Compiling shaders : " << vertexShaderFilePath << " : " << fragmentShaderFilePath << std::endl;
	std::cout << "Compiling shaders : " << std::to_string(vertId) << " : " << std::to_string(fragId) << std::endl;
	vertId = glCreateShader(GL_VERTEX_SHADER);
	fragId = glCreateShader(GL_FRAGMENT_SHADER);

	compileShader(vertexShaderFilePath, vertId);
	compileShader(fragmentShaderFilePath, fragId);

}

void Shader::linkShaders()
{
	programId = glCreateProgram();

	glAttachShader(programId, vertId);
	glAttachShader(programId, fragId);

	glLinkProgram(programId);
	glValidateProgram(programId);

	GLint isLinked = 0;
	glGetProgramiv(programId, GL_LINK_STATUS, (int*)& isLinked);
	if (isLinked == GL_FALSE) {
		std::cout << "Error linking shaders" << std::endl;

		GLint maxLength = 0;
		glGetShaderiv(programId, GL_INFO_LOG_LENGTH, &maxLength);
		std::cout << "error log length = " << std::to_string(maxLength) << std::endl;
		std::vector<char> errorLog(maxLength);
		glGetProgramInfoLog(programId, maxLength, &maxLength, &errorLog[0]);
		
		std::cout << "glgetProgramInfoLog";
		glDeleteProgram(programId);
		glDeleteShader(vertId);
		glDeleteShader(fragId);

		fprintf(stdout, "Error linking shaders");
		// Add logging here at some point
		return;
	}

	std::cout << "Shader linked to id : " << std::to_string(programId) << std::endl;

	glDetachShader(programId, vertId);
	glDetachShader(programId, fragId);
	glDeleteShader(vertId);
	glDeleteShader(fragId);

}

void Shader::compileShader(const std::string& filePath, GLuint& shaderId) {
	
	std::string data = read_file(filePath.c_str(), "rb");
	const GLchar* fileData = data.c_str();
	//const GLchar* fileData = read_file(filePath.c_str()).c_str();
	glShaderSource(shaderId, 1, &fileData, NULL);

	glCompileShader(shaderId);

	GLint success = 0;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);

	if (success == GL_FALSE) {
		GLint maxLength = 0;
		glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<char> errorLog(maxLength);
		glGetShaderInfoLog(shaderId, maxLength, &maxLength, &errorLog[0]);

		glDeleteShader(shaderId);

		fprintf(stdout, "Error compiling shader");
		fprintf(stdout, "%s\n", &(errorLog[0]));

		// Add logging here at some point
		return;
	}
}

void Shader::addAttribute(const std::string& attribName) {
	glBindAttribLocation(programId, numAttributes, attribName.c_str());
	numAttributes++;
}

void Shader::use() const{
	glUseProgram(programId);
	for (int i = 0; i < numAttributes; i++) {
		glEnableVertexAttribArray(i);
	}
}
void Shader::unuse() {
	for (int i = 0; i < numAttributes; i++) {
		glDisableVertexAttribArray(i);
	}
	glUseProgram(0);

}

void Shader::setMat4(const std::string name, Mat4 *matrix) const{
	GLint loc = glGetUniformLocation(programId, name.c_str());
	glUniformMatrix4fv(loc, 1, GL_FALSE, &matrix->elements[0][0]);
}

void Shader::setVec3(const std::string name, Vec3 *position) const{
	GLint loc = glGetUniformLocation(programId, name.c_str());
	if (int err = glGetError()) std::cout << err << std::endl;
	glUniform3f(loc, position->x, position->y, position->z);
	if (int err = glGetError()) {
		std::cout << "glUniform3f()" << err << std::endl;
		std::cout << name << std::endl;
		std::cout << programId << " : " << loc << std::endl;
	}
}

void Shader::setInt(const std::string name, int value) const {
	GLint loc = glGetUniformLocation(programId, name.c_str());
	glUniform1i(loc, value);
}

void Shader::setFloat(const std::string name, float value) const {
	GLint loc = glGetUniformLocation(programId, name.c_str());
	glUniform1f(loc, value);
}

GLuint Shader::getUniformLocation(const std::string uniformName) const{
	GLuint location = glGetUniformLocation(programId, uniformName.c_str());
	if (location == GL_INVALID_INDEX) {
		std::cout << "Error getting uniform location : <" << uniformName << "> from shader : " << std::to_string(programId) << "\n";
	}
	return location;
}

GLuint Shader::getAttribLocation(const std::string attribName) const {
	GLuint location = glGetAttribLocation(programId, attribName.c_str());
	if (location == GL_INVALID_INDEX) {
		std::cout << "Error getting attribute location : <" << attribName << "> from shader : " << std::to_string(programId) << "\n";
	}
	return location;
}
