#pragma once


#include <string>
#include <GL/glew.h>
#include "../../maths/maths.h"
using namespace CariEngine;
using namespace Maths;

class Shader
{
	
public:
	Shader();
	~Shader();

	void compileShaders(const std::string& vertexShaderFilePath, const std::string& fragmentShaderFilePath);

	void linkShaders();


	void addAttribute(const std::string& attribName);

	void use() const;
	void unuse();

	void setMat4(const std::string name, Mat4 *matrix) const;
	void setVec3(const std::string name, Vec3 *position) const;
	void setInt(const std::string name, int value) const;
	void setFloat(const std::string name, float value) const;

	GLuint getUniformLocation(const std::string uniformName) const;
	GLuint getAttribLocation(const std::string attribName) const;

private:

	int numAttributes;

	void compileShader(const std::string& filePath, GLuint& id);

	GLuint programId;
	GLuint vertId;
	GLuint fragId;
};

