#pragma once
#include <vector>
#include "../../maths/maths.h"


namespace CariEngine { namespace Graphics{
	using namespace std;
	using namespace Maths;

	struct Joint {
		int id;
		vector<Joint*> children;
		Joint* parent;

		Mat4 animatedTransform;

		Mat4 boneSpaceTransform;
		Mat4 inverseBoneSpaceTransform;
		
		Joint(int index, Mat4 boneSpaceTransform) {
			this->id = index;
			this->boneSpaceTransform = boneSpaceTransform;
			parent = nullptr;
		}

		void setParent(Joint* parent) {
			this->parent = parent;
		}

		void addChild(Joint* child) {
			children.push_back(child);
			child->setParent(this);
		}
	};


}}