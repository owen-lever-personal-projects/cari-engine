#pragma once

#include <GL/glew.h>
#include "../buffers/VertexArray.h"
#include "../buffers/IndexBuffer.h"
#include "../shaders/Shader.h"
#include "../objects/VertexData.h"
#include "../renderables/RenderableGuiElement.h"

namespace CariEngine {
	namespace Graphics {
#define RENDERER_MAX_GUI_ELEMENTS 20000
#define RENDERER_VERTEX_SIZE sizeof(Vertex2D)
#define RENDERER_ELEMENT_SIZE RENDERER_VERTEX_SIZE * 4
#define RENDERER_BUFFER_SIZE RENDERER_ELEMENT_SIZE * RENDERER_MAX_GUI_ELEMENTS
#define RENDERER_INDICES_SIZE RENDERER_MAX_GUI_ELEMENTS * 6


		class GuiRenderer {
		public:
			GuiRenderer(Shader* shader);
			~GuiRenderer();

			void begin();
			void end();

			void draw(const RenderableGuiElement* renderable);
			void flush();



		private:
			void init();
			Vertex2D* buffer;

			GLuint vao;
			GLuint vbo;
			IndexBuffer* ibo;
			GLsizei indexCount;
			const Shader* shader;



		};




	}
}