#pragma once

#include "AbstractRenderer2D.h"
#include <GL/glew.h>
#include "../buffers/VertexArray.h"
#include "../buffers/IndexBuffer.h"
#include "../shaders/Shader.h"
#include "../objects/VertexData.h"

namespace CariEngine { namespace Graphics {
#define RENDERER_MAX_SPRITES 60000
#define RENDERER_VERTEX_SIZE sizeof(Vertex2D)
#define RENDERER_SPRITE_SIZE RENDERER_VERTEX_SIZE * 4
#define RENDERER_BUFFER_SIZE RENDERER_SPRITE_SIZE * RENDERER_MAX_SPRITES
#define RENDERER_INDICES_SIZE RENDERER_MAX_SPRITES * 6


	class BatchRenderer2D : public AbstractRenderer2D {
	public:
		BatchRenderer2D(Shader* shader);
		~BatchRenderer2D();

		void begin() override;
		void end() override;

		void draw(const Renderable2D* renderable) override;
		void flush() override;



	private:
		void init();
		Vertex2D* buffer;

		GLuint vao;
		GLuint vbo;
		IndexBuffer* ibo;
		GLsizei indexCount;
		const Shader* shader;



	};




}}