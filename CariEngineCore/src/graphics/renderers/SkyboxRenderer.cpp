#include "SkyboxRenderer.h"

namespace CariEngine { namespace Graphics {


	
	void SkyboxRenderer::draw(CubeMap* cubemap, Mat4* projection, Mat4* view) {
		shader->use();

		shader->setMat4("pr_matrix", projection);
		shader->setMat4("vw_matrix", view);

		
		glBindVertexArray(vao);
		cubemap->bind();
		glDrawArrays(GL_TRIANGLES, 0, 36);


	}

	SkyboxRenderer::SkyboxRenderer(Shader* shader) {
		this->shader = shader;

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		float vertices[] = {
			-1.0f,  1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			 1.0f, -1.0f, -1.0f,
			 1.0f, -1.0f, -1.0f,
			 1.0f,  1.0f, -1.0f,
			-1.0f,  1.0f, -1.0f,

			-1.0f, -1.0f,  1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f,  1.0f, -1.0f,
			-1.0f,  1.0f, -1.0f,
			-1.0f,  1.0f,  1.0f,
			-1.0f, -1.0f,  1.0f,

			 1.0f, -1.0f, -1.0f,
			 1.0f, -1.0f,  1.0f,
			 1.0f,  1.0f,  1.0f,
			 1.0f,  1.0f,  1.0f,
			 1.0f,  1.0f, -1.0f,
			 1.0f, -1.0f, -1.0f,

			-1.0f, -1.0f,  1.0f,
			-1.0f,  1.0f,  1.0f,
			 1.0f,  1.0f,  1.0f,
			 1.0f,  1.0f,  1.0f,
			 1.0f, -1.0f,  1.0f,
			-1.0f, -1.0f,  1.0f,

			-1.0f,  1.0f, -1.0f,
			 1.0f,  1.0f, -1.0f,
			 1.0f,  1.0f,  1.0f,
			 1.0f,  1.0f,  1.0f,
			-1.0f,  1.0f,  1.0f,
			-1.0f,  1.0f, -1.0f,

			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f,  1.0f,
			 1.0f, -1.0f, -1.0f,
			 1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f,  1.0f,
			 1.0f, -1.0f,  1.0f
		};

		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);

		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), &vertices[0], GL_STATIC_DRAW);

		std::cout << sizeof(vertices) << std::endl;

		GLuint position = shader->getAttribLocation("position");
		glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(position);


		glBindBuffer(GL_ARRAY_BUFFER, 0);

	}





}}