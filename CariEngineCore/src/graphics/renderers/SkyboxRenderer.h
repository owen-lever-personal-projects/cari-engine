#pragma once

#include "../shaders/Shader.h"
#include "../textures/CubeMap.h"
#include "GL/glew.h"
#include "../../maths/maths.h"

namespace CariEngine { namespace Graphics {

	class SkyboxRenderer {

		GLuint vao;
		GLuint vbo;

		Shader* shader;

	public:
		SkyboxRenderer(Shader* shader);

		void draw(CubeMap* cubemap, Mat4* projection, Mat4* view);

	};





}}