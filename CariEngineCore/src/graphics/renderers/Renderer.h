#pragma once

#include "../renderables/Renderable2D.h"
#include "../renderables/Renderable3D.h"
#include "../../maths/maths.h"
#include "../textures/CubeMap.h"
#include <deque>
#include "../objects/Light.h"
#include "../objects/Camera.h"
#include "../renderables/RenderableText.h"
#include "../../core/Window.h"

namespace CariEngine { namespace Graphics {

	class Renderer {
	public:

		Shader standardShader;
		Shader skyShader;
		Shader guiShader;
		Shader shadowShader;
		Shader shader2d;

		Renderer();
		~Renderer();
		void begin();
		void end();

		void draw(Renderable2D* renderable);
		void draw(Renderable3D* renderable);
		//void draw(GameObject* object);
		void draw(RenderableText* text);

		void addLight(PointLight* light);
		void addLight(DirectionalLight* light);

		void setCubemap( CubeMap* cubemap);
		void setProjectionMatrix( Mat4* matrix);
		void setViewMatrix( Mat4* matrix);
		void setCamera(Camera* camera);

		void resize(int width, int height);

		void flush();
	private:
		int width;
		int height;
		Camera* camera;

		void Render3D(Shader* shader);
		void Render2D(Shader* shader);
		void SetupLights(Shader* shader);
		void RenderGui(Shader* shader);
		void ResetBuffers();
		void RenderDirLight(Shader* shader, DirectionalLight* light, unsigned int fbo);


	};






}}