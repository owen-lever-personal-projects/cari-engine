#include "BatchRenderer2D.h"
#include <iostream>
#include "../objects/VertexData.h"

namespace CariEngine { namespace Graphics {

	BatchRenderer2D::BatchRenderer2D(Shader* shader) {
		this->shader = shader;
		init();
	}
	
	void BatchRenderer2D::init() {
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, RENDERER_BUFFER_SIZE, NULL, GL_DYNAMIC_DRAW);

		std::cout << "2d vao :" << std::to_string(vbo) << std::endl;

		GLuint position = shader->getAttribLocation("position");
		glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, RENDERER_VERTEX_SIZE, (const GLvoid*)offsetof(Vertex2D, position));
		glEnableVertexAttribArray(position);

		GLuint color = shader->getAttribLocation("color");
		glVertexAttribPointer(color, 4, GL_FLOAT, GL_TRUE, RENDERER_VERTEX_SIZE, (const GLvoid*)offsetof(Vertex2D, color));
		glEnableVertexAttribArray(color);

		GLuint uv = shader->getAttribLocation("uv");
		glVertexAttribPointer(uv, 2, GL_FLOAT, GL_TRUE, RENDERER_VERTEX_SIZE, (const GLvoid*)offsetof(Vertex2D, uv));
		glEnableVertexAttribArray(uv);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		GLushort indices[RENDERER_INDICES_SIZE];
		for (int i = 0; i < RENDERER_MAX_SPRITES; i++) {
			indices[i * 6 + 0] = (i * 4 + 0);
			indices[i * 6 + 1] = (i * 4 + 3);
			indices[i * 6 + 2] = (i * 4 + 1);
			indices[i * 6 + 3] = (i * 4 + 0);
			indices[i * 6 + 4] = (i * 4 + 2);
			indices[i * 6 + 5] = (i * 4 + 3);
		}

		ibo = new IndexBuffer(indices, RENDERER_INDICES_SIZE);
		glBindVertexArray(0);

	}

	void BatchRenderer2D::draw(const Renderable2D* renderable) {
		//Position = Bottom left
		const Vec3& position = renderable->getPosition();
		const Vec2& size = renderable->getSize();
		const Vec4& color = renderable->getColor();

		//Bottom left
		buffer->position = Vec3(position.x			, position.y			, position.z);
		buffer->color = color;
		buffer->uv = Vec2(0, 0);
		buffer++;

		//Top left
		buffer->position = Vec3(position.x			, position.y - size.y	, position.z);
		buffer->color = color;
		buffer->uv = Vec2(0, 1);
		buffer++;

		//Bottom right
		buffer->position = Vec3(position.x + size.x	, position.y			, position.z);
		buffer->color = color;
		buffer->uv = Vec2(1, 0);
		buffer++;

		//Top right
		buffer->position = Vec3(position.x + size.x	, position.y - size.y	, position.z);
		buffer->color = color;
		buffer->uv = Vec2(1, 1);
		buffer++;

		indexCount += 6;
	}

	void BatchRenderer2D::begin() {
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		
		buffer = (Vertex2D*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
		if (buffer == nullptr) {
			std::cout << "NULL";
		}
		indexCount = 0;
	}

	void BatchRenderer2D::end() {
		glUnmapBuffer(GL_ARRAY_BUFFER);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void BatchRenderer2D::flush() {
		glBindVertexArray(vao);
		ibo->bind();

		glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_SHORT, NULL);

		ibo->unbind();
		glBindVertexArray(0);
	}

	BatchRenderer2D::~BatchRenderer2D() {
		delete ibo;
		glDeleteBuffers(1, &vbo);
	}
}}