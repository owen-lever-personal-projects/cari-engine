#include "GuiRenderer.h"
#include <iostream>
#include "../objects/VertexData.h"
#include <vector>

namespace CariEngine {
	namespace Graphics {

		GuiRenderer::GuiRenderer(Shader* shader) {
			this->shader = shader;
			init();
		}

		void GuiRenderer::init() {
			glGenVertexArrays(1, &vao);
			glBindVertexArray(vao);

			glGenBuffers(1, &vbo);
			glBindBuffer(GL_ARRAY_BUFFER, vbo);
			glBufferData(GL_ARRAY_BUFFER, RENDERER_BUFFER_SIZE, NULL, GL_DYNAMIC_DRAW);

			std::cout << "gui vao :" << std::to_string(vbo) << std::endl;

			GLuint position = shader->getAttribLocation("position");
			glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, RENDERER_VERTEX_SIZE, (const GLvoid*)offsetof(Vertex2D, position));
			glEnableVertexAttribArray(position);

			GLuint color = shader->getAttribLocation("color");
			glVertexAttribPointer(color, 4, GL_FLOAT, GL_TRUE, RENDERER_VERTEX_SIZE, (const GLvoid*)offsetof(Vertex2D, color));
			glEnableVertexAttribArray(color);

			GLuint uv = shader->getAttribLocation("uv");
			glVertexAttribPointer(uv, 2, GL_FLOAT, GL_TRUE, RENDERER_VERTEX_SIZE, (const GLvoid*)offsetof(Vertex2D, uv));
			glEnableVertexAttribArray(uv);

			glBindBuffer(GL_ARRAY_BUFFER, 0);

			GLushort indices[RENDERER_INDICES_SIZE];
			for (int i = 0; i < RENDERER_MAX_GUI_ELEMENTS; i++) {
				indices[i * 6 + 0] = (i * 4 + 0);
				indices[i * 6 + 1] = (i * 4 + 3);
				indices[i * 6 + 2] = (i * 4 + 1);
				indices[i * 6 + 3] = (i * 4 + 0);
				indices[i * 6 + 4] = (i * 4 + 2);
				indices[i * 6 + 5] = (i * 4 + 3);
			}

			ibo = new IndexBuffer(indices, RENDERER_INDICES_SIZE);
			glBindVertexArray(0);

		}

		void GuiRenderer::draw(const RenderableGuiElement* gui) {
			//Position = Top left
			const Vec2& position = gui->position;
			const Vec2& size = gui->size;
			const Vec4& color = gui->color;

			//Bottom left
			buffer->position = Vec3(position.x, position.y + size.y, 0);
			buffer->color = color;
			buffer->uv = Vec2(gui->uvPos.x, gui->uvPos.y + gui->uvSize.y);
			buffer++;

			//Top left
			buffer->position = Vec3(position.x, position.y, 0);
			buffer->color = color;
			buffer->uv = Vec2(gui->uvPos.x, gui->uvPos.y);
			buffer++;

			//Bottom right
			buffer->position = Vec3(position.x + size.x, position.y + size.y, 0);
			buffer->color = color;
			buffer->uv = Vec2(gui->uvPos.x + gui->uvSize.x, gui->uvPos.y + gui->uvSize.y);
			buffer++;

			//Top right
			buffer->position = Vec3(position.x + size.x, position.y, 0);
			buffer->color = color;
			buffer->uv = Vec2(gui->uvPos.x + gui->uvSize.x, gui->uvPos.y);
			buffer++;

			indexCount += 6;
		}

		void GuiRenderer::begin() {
			
			glBindBuffer(GL_ARRAY_BUFFER, vbo);

			buffer = (Vertex2D*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
			if (buffer == nullptr) {
				std::cout << "GUI Renderer Buffer = NULL";
			}
			indexCount = 0;
		}

		void GuiRenderer::end() {
			glUnmapBuffer(GL_ARRAY_BUFFER);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}

		void GuiRenderer::flush() {
			glBindVertexArray(vao);
			ibo->bind();

			glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_SHORT, NULL);

			ibo->unbind();
			glBindVertexArray(0);
		}

		GuiRenderer::~GuiRenderer() {
			delete ibo;
			glDeleteBuffers(1, &vbo);
		}
	}
}