#pragma once
#include "../renderables/Renderable2D.h"
#include "../renderables/Renderable3D.h"
#include <GL/glew.h>
#include "../../maths/maths.h"

namespace CariEngine {
	namespace Graphics {

		class AbstractFullRenderer {
		public:
			virtual void draw(const Renderable2D* renderable) = 0;
			virtual void draw(const Renderable3D* renderable) = 0;
			virtual void flush() = 0;
			virtual void begin() = 0;
			virtual void end() = 0;

		};




	}
}