#include "Renderer.h"

#include "BatchRenderer2D.h"
#include "../shaders/Shader.h"
#include "SkyboxRenderer.h"
#include "../textures/CubeMap.h"
#include <map>
#include "../objects/Model.h"
#include "../fonts/Font.h"
#include "GuiRenderer.h"

namespace CariEngine {
	namespace Graphics {

		unsigned int depthMapFBO;
		unsigned int depthMapTexture;

		unsigned int shadowResolution = 4096;


		std::map<Model*, std::deque<Renderable3D*>> models;
		std::map<Font*, std::deque<RenderableText*>> textRenderables;

		std::deque<PointLight*> pointLights;
		std::deque<DirectionalLight*> directionalLights;
		std::vector<Renderable2D*> renderable2Ds;
		int amountOfPointLights;
		int amountOfDirectionalLights;

		AbstractRenderer2D* renderer2D;
		GuiRenderer* guiRenderer;

		SkyboxRenderer* skyRenderer;
		CubeMap* skybox;
		Mat4 proj;
		Mat4 view;

		Vec4 skyColor(0.737, 0.886, 1, 1);


		Renderer::~Renderer() {

		}

		Renderer::Renderer(){

			// Enables MSAA
			glEnable(GL_MULTISAMPLE);

			// Enables alpha blending (transparency)
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

			// Enables culling
			glEnable(GL_CULL_FACE);
			glFrontFace(GL_CCW);
			glCullFace(GL_BACK);

			// Enables depth testing
			glEnable(GL_DEPTH_TEST);
			glDepthMask(GL_TRUE);
			glDepthFunc(GL_LEQUAL);
			glDepthRange(0.0f, 1.0f);
			glClearDepth(1.0f);

			// Enables alpha testing (Discarding of fragments that don't have greater than 0 alpha)
			glAlphaFunc(GL_GREATER, 0);
			glEnable(GL_ALPHA_TEST);

			// Compiles and links all of the shaders needed for the renderer
			shader2d.compileShaders("src/shaders/2d.vs", "src/shaders/2d.fs");
			shader2d.linkShaders();

			shadowShader.compileShaders("src/shaders/shadow.vs", "src/shaders/shadow.fs");
			shadowShader.linkShaders();

			skyShader.compileShaders("src/shaders/skybox.vs", "src/shaders/skybox.fs");
			skyShader.linkShaders();

			guiShader.compileShaders("src/shaders/gui.vs", "src/shaders/gui.fs");
			guiShader.linkShaders();

			standardShader.compileShaders("src/shaders/standard.vs", "src/shaders/standard.fs");
			standardShader.linkShaders();

			// Initialises all of the renderers
			skyRenderer = new SkyboxRenderer(&skyShader);
			guiRenderer = new GuiRenderer(&guiShader);
			renderer2D = new BatchRenderer2D(&shader2d);
			
			// Sets the width and height values to the default resolution
			this->width = 800;
			this->height = 450;
			
			// Intialises the camera pointer to null
			camera = nullptr;

			// Generates the frame buffer used for the directional light's shadow map
			glGenFramebuffers(1, &depthMapFBO);
			glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);

			// Generates the shadow map texture
			glGenTextures(1, &depthMapTexture);
			glBindTexture(GL_TEXTURE_2D, depthMapTexture);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, shadowResolution, shadowResolution, 0, GL_RGBA, GL_FLOAT, NULL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
			
			// Attaches the shadow map texture to the frame buffer
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, depthMapTexture, 0);
			
			
			//glDrawBuffer(GL_NONE);
			//glReadBuffer(GL_NONE);

			// Generates a render buffer used by the gpu to perform depth tests when rendering to the 
			unsigned int rbo;
			glGenRenderbuffers(1, &rbo);
			glBindRenderbuffer(GL_RENDERBUFFER, rbo);
			glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, shadowResolution, shadowResolution);
			glBindRenderbuffer(GL_RENDERBUFFER, 0);

			// Attached the render buffer to the frame buffer
			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);

			// Checks to make sure the frame buffer has been created properly
			int error = glCheckFramebufferStatus(GL_FRAMEBUFFER);
			if (error != GL_FRAMEBUFFER_COMPLETE) {
				std::cout << "FRAME BUFFER ERROR " << error << std::endl;
			}

			// Binds the default frame buffer and texture after the shadowmap has been setup
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			glBindTexture(GL_TEXTURE_2D, 0);
			standardShader.use();

			// Sets the uniform location of "tex" and "shadowMap" to 0 and 1 in the gpu
			int t0 = standardShader.getUniformLocation("tex");
			glUniform1i(t0, 0);

			int t1 = standardShader.getUniformLocation("shadowMap");
			glUniform1i(t1, 1);
		}

		void Renderer::begin() {
		}

		void Renderer::end() {
		}

		void Renderer::draw(Renderable2D* renderable) {
			renderable2Ds.push_back(renderable);
		}

		void Renderer::draw(Renderable3D* renderable) {
			models[renderable->model].push_back(renderable);
		}

		void Renderer::draw(RenderableText* text) {
			textRenderables[text->font].push_back(text);
		}

		void Renderer::addLight(PointLight* light) {
			amountOfPointLights++;
			pointLights.push_back(light);
		}

		void Renderer::addLight(DirectionalLight* light) {
			amountOfDirectionalLights++;
			directionalLights.push_back(light);
		}

		void Renderer::setCubemap(CubeMap* cubemap) {
			skybox = cubemap;
		}

		void Renderer::setProjectionMatrix(Mat4* matrix) {
			proj = *matrix;
		}

		void Renderer::setViewMatrix(Mat4* matrix) {
			view = *matrix;
		}

		void Renderer::setCamera(Camera* camera) {
			this->camera = camera;
		}

		void Renderer::resize(int width, int height) {
			this->width = width;
			this->height = height;
		}




		void Renderer::flush() {
			glEnable(GL_DEPTH_TEST);
			
			// Doesn't draw the skybox (Sets the viewPos uniform used in lighting calculations)
			standardShader.setVec3("viewPos", camera ? &camera->getPosition() : &Vec3());
			
			// Also doesn't draw the skybox (Sets the view matrix)
			if (camera)	setViewMatrix(&camera->getView());

			// Draws the skybox
			if(skybox) skyRenderer->draw(skybox, &proj, &view);


			if (!directionalLights.empty()) {
				RenderDirLight(&shadowShader, directionalLights[0], depthMapFBO);
			}

			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, depthMapTexture);
			glActiveTexture(GL_TEXTURE0);


			standardShader.use();
			SetupLights(&standardShader);

			glViewport(0, 0, width, height);
			standardShader.setMat4("vw_matrix", &view);
			standardShader.setMat4("pr_matrix", &proj);
			Render3D(&standardShader);

			glBindTexture(GL_TEXTURE_2D, depthMapTexture);

			/*
			Texture tex;
			
			tex.height = shadowResolution;
			tex.width = shadowResolution;
			tex.id = depthMapTexture;
			Renderable2D r2d = Renderable2D(Vec3(100, 700, 1), Vec2(500, 500), Vec4(1, 1, 1, 1), &tex);
			draw(&r2d);
			*/
			
			glDisable(GL_DEPTH_TEST);

			Render2D(&shader2d);
			RenderGui(&standardShader);
			ResetBuffers();
			standardShader.use();
			glClearColor(skyColor.x, skyColor.y, skyColor.z, skyColor.w);
		}

		void Renderer::RenderDirLight(Shader* shader, DirectionalLight* light, unsigned int fbo) {
			glClearColor(1, 1, 1, 1);
			glBindFramebuffer(GL_FRAMEBUFFER, fbo);
			glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
			glViewport(0, 0, shadowResolution, shadowResolution);

			Mat4 lightProjection = Mat4::ortho(400, -400, -400, 400, -400, 400);
			//Mat4 lightProjection = Mat4::persp(60, 1, 1, 1000);
			Vec3 dir = Vec3::normalised(light->direction);
			Vec3 pos = camera->position;
			Mat4 lightView = Mat4::lookAt(pos, pos + dir, Vec3(0, 1, 0));
			
			shader->use();
			//glDisable(GL_CULL_FACE);
			//glCullFace(GL_FRONT);
			
			Mat4 mvMatrix = lightProjection * lightView;
			shader->setMat4("pr_matrix", &lightProjection);
			shader->setMat4("vw_matrix", &lightView);
			shader->setVec3("lightDir", &light->direction);

			Render3D(shader);

			standardShader.use();
			standardShader.setMat4("shadow_pr_matrix", &lightProjection);
			standardShader.setMat4("shadow_vw_matrix", &lightView);
			glCullFace(GL_BACK);
			glEnable(GL_CULL_FACE);
			glBindFramebuffer(GL_FRAMEBUFFER, 0);

		}

		void Renderer::SetupLights(Shader* shader) {
			standardShader.use();
			standardShader.setInt("amountOfPointLights", amountOfPointLights);
			standardShader.setInt("amountOfDirectionalLights", amountOfDirectionalLights);



			while (!pointLights.empty()) {
				amountOfPointLights--;
				PointLight* light = pointLights.back();

				std::string name = "pointLights[" + std::to_string(amountOfPointLights) + "].";

				standardShader.setVec3(name + "position", &light->position);
				standardShader.setVec3(name + "ambient", &light->ambient);
				standardShader.setVec3(name + "diffuse", &light->diffuse);
				standardShader.setVec3(name + "specular", &light->specular);

				standardShader.setFloat(name + "constant", light->constant);
				standardShader.setFloat(name + "linear", light->linear);
				standardShader.setFloat(name + "quadratic", light->quadratic);

				pointLights.pop_back();
			}

			while (!directionalLights.empty()) {
				amountOfDirectionalLights--;
				DirectionalLight* light = directionalLights.back();

				std::string name = "directionalLights[" + std::to_string(amountOfPointLights) + "].";

				light->direction.normalise();
				standardShader.setVec3(name + "direction", &light->direction);
				standardShader.setVec3(name + "ambient", &light->ambient);
				standardShader.setVec3(name + "diffuse", &light->diffuse);
				standardShader.setVec3(name + "specular", &light->specular);

				directionalLights.pop_back();
			}

			amountOfPointLights = 0;
			amountOfDirectionalLights = 0;
		}

		void Renderer::Render3D(Shader* shader) {

			for (std::map<Model*, std::deque<Renderable3D*>>::iterator it = models.begin(); it != models.end(); it++) {
				Model* model = it->first;
				std::deque<Renderable3D*>& queue = models[model];
				for (int i = 0; i < queue.size(); i++) {
					Renderable3D* renderable = queue[i];
					Mat4 matrix = Mat4::scale(renderable->scale);
					matrix.multiply(Mat4::rotation(renderable->roll, renderable->pitch, renderable->yaw));
					matrix.multiply(Mat4::translation(renderable->position));
					shader->setMat4("ml_matrix", &matrix);

					renderable->draw();
				}
			}

		}

		void Renderer::Render2D(Shader* shader) {
			renderer2D->begin();
			shader->use();
			shader->setMat4("pr_matrix", &Mat4::ortho(0, height, 0, width, -1, 1));
			for (int i = 0; i < renderable2Ds.size(); i++) {
				Renderable2D* renderable = renderable2Ds[i];
				renderable->getTexture()->bind();
				renderer2D->draw(renderable);
			}
			renderer2D->end();
			renderer2D->flush();
		}

		void Renderer::RenderGui(Shader* shader) {
			guiRenderer->begin();
			guiShader.use();
			guiShader.setMat4("pr_matrix", &Mat4::ortho(0, height, 0, width, -1, 1));
			
			for (std::map<Font*, std::deque<RenderableText*>>::iterator it = textRenderables.begin(); it != textRenderables.end(); it++) {
				Font* font = it->first;
				font->pages[0].texture->bind();
				std::deque<RenderableText*>* queue = &textRenderables[font];

				while (!queue->empty()) {
					RenderableText* text = queue->back();
					float scale = 1;
					if (text->size > 0) {
						scale = (text->size / text->font->size);
					}

					Vec2 cursor = text->position;
					for (char c : text->text) {

						FontCharacter* fc;
						if (font->characters.find(c) == font->characters.end()) {
							fc = &font->characters['?'];
						}
						else {
							fc = &font->characters[c];
						}

						Vec2 position = cursor + (fc->offset * scale);
						Vec2 size = fc->size * scale;

						Vec2 texSize = Vec2(font->pages[0].texture->width, font->pages[0].texture->height);
						Vec2 uvPos = fc->position / texSize;
						Vec2 uvSize = fc->size / texSize;

						RenderableGuiElement ge(position, size, Vec4(1, 1, 1, 1), uvPos, uvSize);
						guiRenderer->draw(&ge);

						cursor.x += fc->xAdvance * scale;
					}
					queue->pop_back();
				}

			}

			guiRenderer->end();
			guiRenderer->flush();
		}

		void Renderer::ResetBuffers() {
			renderable2Ds.clear();
			models.clear();
			textRenderables.clear();

			pointLights.clear();
			directionalLights.clear();
		}
	}
}