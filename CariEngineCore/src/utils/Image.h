#pragma once
#include <string>
#include <FI/FreeImage.h>
#include <iostream>

namespace CariEngine {
	class Image {

	public:
		Image(std::string filepath, bool flipped = false);
		~Image();

		int height;
		int width;
		BYTE* data;

	private:
		void load(std::string filepath, bool flipped);
		void unload();








	};




}