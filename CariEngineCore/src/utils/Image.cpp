#include "Image.h"


namespace CariEngine {

	FIBITMAP* dib(0);




	void Image::load(std::string filePath, bool flipped) {
		//image format
		FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
		//pointer to the image, once loaded
		

		//check the file signature and deduce its format
		fif = FreeImage_GetFileType(filePath.c_str(), 0);
		//if still unknown, try to guess the file format from the file extension
		if (fif == FIF_UNKNOWN)
			fif = FreeImage_GetFIFFromFilename(filePath.c_str());
		//if still unkown, return failure
		if (fif == FIF_UNKNOWN) {
			std::cout << "Unknown file format!" << std::endl;
			return;

		}

		//check that the plugin has reading capabilities and load the file
		if (FreeImage_FIFSupportsReading(fif))
			dib = FreeImage_Load(fif, filePath.c_str());
		//if the image failed to load, return failure
		if (!dib) {
			std::cout << "Image Failed to load!" << std::endl;
			std::cout << filePath << std::endl;
			return;
		}

		dib = FreeImage_ConvertTo32Bits(dib);
		if (flipped) {

			FreeImage_FlipVertical(dib);
		}

		//retrieve the image data
		data = FreeImage_GetBits(dib);
		//get the image width and height
		width = FreeImage_GetWidth(dib);
		height = FreeImage_GetHeight(dib);

		
	}

	void Image::unload() {
		FreeImage_Unload(dib);
	}

	Image::Image(std::string filePath, bool flipped) {
		load(filePath, flipped);
	}

	Image::~Image() {
		unload();
	}
}