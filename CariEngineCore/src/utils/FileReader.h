#pragma once
#include <string>
#include <iostream>
#include <fstream>



namespace CariEngine {

	static std::string read_file(std::string filePath, std::string mode = "rt") {
		return read_file(filePath.c_str(), mode.c_str());
	}

	static std::string read_file(const char* filePath, const char* mode = "rt") {
		FILE* file = fopen(filePath, mode);
		fseek(file, 0, SEEK_END);
		unsigned long length = ftell(file);
		char* data = new char[length + 1];
		memset(data, '\0', length + 1);
		fseek(file, 0, SEEK_SET);
		fread(data, 1, length, file);
		fclose(file);
		std::string result(data);
		delete[] data;

		return result;
	}
	


}