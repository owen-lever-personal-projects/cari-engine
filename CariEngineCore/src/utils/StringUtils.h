#pragma once
#include <string>
#include <vector>

namespace CariEngine {
	using namespace std;
	
	vector<string> split_string(string s, string split) {
		bool done = false;
		std::vector<string> result;
		while (!done) {
			int index = s.find(split);
			if (index == -1) {
				done = true;
				result.push_back(s);
			}
			else {
				if (index != 0)
					result.push_back(s.substr(0, index));
				s.erase(0, index + split.size());
			}
		}

		return result;
	}


}