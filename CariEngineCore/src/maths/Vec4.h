#pragma once
#include <iostream>

namespace CariEngine { namespace Maths {
	struct Vec4 
	{
		float x;
		float y;
		float z;
		float w;

		Vec4() = default;
		Vec4(const float& x, const float& y, const float& z, const float& w);

		Vec4& add(Vec4 other);
		Vec4& subtract(Vec4 other);
		Vec4& multiply(Vec4 other);
		Vec4& divide(Vec4 other);

		Vec4& normalise();
		float length() const;

		static Vec4 cross(const Vec4& a, const Vec4& b);
		static float dot(const Vec4& a, const Vec4& b);
		static Vec4 normalised(const Vec4& a);

		friend std::ostream& operator<<(std::ostream& stream, const Vec4& vector);

		Vec4& operator+=(const Vec4& right);
		Vec4& operator*=(const Vec4& right);
		Vec4& operator-=(const Vec4& right);
		Vec4& operator/=(const Vec4& right);

		friend Vec4 operator+(const Vec4& left, const Vec4& right);
		friend Vec4 operator*(const Vec4& left, const Vec4& right);
		friend Vec4 operator-(const Vec4& left, const Vec4& right);
		friend Vec4 operator/(const Vec4& left, const Vec4& right);

		friend Vec4 operator*(const Vec4& left, const float& right);
		friend Vec4 operator/(const Vec4& left, const float& right);

		friend bool operator<(const Vec4& left, const Vec4& right);
		friend bool operator==(const Vec4& left, const Vec4& right);


	};




}}