#include "Vec3.h"
#include <string>
#include <cmath>
#include "maths.h"

namespace CariEngine {
	namespace Maths {

		Vec3::Vec3() {
			this->x = 0;
			this->y = 0;
			this->z = 0;
		}

		Vec3::Vec3(const float& x, const float& y, const float& z) {
			this->x = x;
			this->y = y;
			this->z = z;
		}

		Vec3::Vec3(const float& v) {
			this->x = v;
			this->y = v;
			this->z = v;
		}

		Vec3& Vec3::add(const Vec3& other) {
			x += other.x;
			y += other.y;
			z += other.z;
			return *this;
		}
		Vec3& Vec3::subtract(const Vec3& other) {
			x -= other.x;
			y -= other.y;
			z -= other.z;
			return *this;
		}
		Vec3& Vec3::multiply(const Vec3& other) {
			x *= other.x;
			y *= other.y;
			z *= other.z;
			return *this;
		}
		Vec3& Vec3::divide(const Vec3& other) {
			x /= other.x;
			y /= other.y;
			z /= other.z;
			return *this;
		}
		Vec3& Vec3::multiply(const float& other) {
			x *= other;
			y *= other;
			z *= other;
			return *this;
		}
		Vec3& Vec3::divide(const float& other) {
			x /= other;
			y /= other;
			z /= other;
			return *this;
		}

		Vec3& Vec3::normalise() {
			float length = this->length();
			x /= length;
			y /= length;
			z /= length;
			return *this;
		}

		float Vec3::length() const {
			return sqrt(x * x + y * y + z * z);
		}

		Vec3 Vec3::cross(const Vec3& a, const Vec3& b) {
			Vec3 result;
			result.x = (a.y * b.z) - (a.z * b.y);
			result.y = (a.z * b.x) - (a.x * b.z);
			result.z = (a.x * b.y) - (a.y * b.x);
			return result;
		};

		float Vec3::dot(const Vec3& a, const Vec3& b) {

			return (a.x * b.x) + (a.y * b.y) + (a.z * b.z);
		}

		Vec3 Vec3::max(const Vec3& a, const Vec3& b) {
			return Vec3(maxf(a.x, b.x), maxf(a.y, b.y), maxf(a.z, b.z));
		}

		Vec3 Vec3::normalised(const Vec3& a) {
			Vec3 result = a;
			result.normalise();
			return result;
		}

		float Vec3::distance(const Vec3& a, const Vec3& b) {
			float x = a.x - b.x;
			float y = a.y - b.y;
			float z = a.z - b.z;

			return sqrt(x * x + y * y + z * z);
		}

		Vec3 Vec3::rotate(const Vec3& vector, const Vec3& axis, const float& angle){
			float length = vector.length();
			
			Vec3 nVec = Vec3::normalised(vector);
			Vec3 nAxis = Vec3::normalised(axis);



			Vec3 par = nAxis * Vec3::dot(nAxis, nVec);
			Vec3 perp = nVec - par;

			Vec3 newPerp = perp * cos(angle) + cross(nAxis, perp) * sin(angle);
			Vec3 newVec = newPerp + par;
			newVec *= length;
			return newVec;
		}

		Vec3 Vec3::getDirectionDegrees(float hRotation, float vRotation) {
			return getDirection(toRadians(hRotation), toRadians(vRotation));
		}

		Vec3 Vec3::getDirection(float hRotation, float vRotation) {
			float x = cos(hRotation);
			float z = sin(hRotation);

			float y = sin(vRotation);
			float m = cos(vRotation);

			x *= m;
			z *= m;

			return Vec3(x, y, z);
		}

		Vec3 operator+(const Vec3& left, const Vec3& right) {
			Vec3 r = left;
			r.add(right);
			return r;
		}

		Vec3 operator*(const Vec3& left, const Vec3& right) {
			Vec3 r = left;
			r.multiply(right);
			return r;
		}

		Vec3 operator-(const Vec3& left, const Vec3& right) {
			Vec3 r = left;
			r.subtract(right);
			return r;
		}

		Vec3 operator/(const Vec3& left, const Vec3& right) {
			Vec3 r = left;
			r.divide(right);
			return r;
		}

		Vec3 operator*(const Vec3& left, const float& right) {
			Vec3 r = left;
			r.x *= right;
			r.y *= right;
			r.z *= right;
			return r;
		}

		Vec3 operator/(const Vec3& left, const float& right) {
			Vec3 r = left;
			r.x /= right;
			r.y /= right;
			r.z /= right;
			return r;
		}
		
		Vec3& Vec3::operator+=(const Vec3& right) {
			return add(right);
		}

		Vec3& Vec3::operator*=(const Vec3& right) {
			return multiply(right);
		}

		Vec3& Vec3::operator-=(const Vec3& right) {
			return subtract(right);
		}

		Vec3& Vec3::operator/=(const Vec3& right) {
			return divide(right);
		}

		Vec3& Vec3::operator*=(const float& right) {
			return multiply(right);
		}

		Vec3& Vec3::operator/=(const float& right) {
			return divide(right);
		}



		bool operator<(const Vec3& left, const Vec3& right) {
			if(left.x != right.x)
				return(left.x < right.x);
			
			if (left.y != right.y)
				return(left.y < right.y);

			return(left.z < right.z);
		}

		bool operator>(const Vec3& left, const Vec3& right) {
			if (left.x != right.x)
				return(left.x > right.x);

			if (left.y != right.y)
				return(left.y > right.y);

			return(left.z > right.z);
		}

		bool operator<=(const Vec3& left, const Vec3& right) {
			return (left == right | left < right);
		}

		bool operator>=(const Vec3& left, const Vec3& right) {
			return (left == right | left > right);
		}



		bool operator==(const Vec3& left, const Vec3& right) {
			return((left.x == right.x) & (left.y == right.y) & (left.z == right.z));
		}

		
		
		std::ostream& operator<<(std::ostream& stream, const Vec3& vector) {
			stream << vector.toString();
			return stream;
		}

		std::string Vec3::toString() const{
			return std::to_string(x) + " : " + std::to_string(y) + " : " + std::to_string(z);
		}

	}
}