#include "Vec4.h"
#include <string>

namespace CariEngine { namespace Maths {
	
	//Vec4::Vec4() {
	//	this->x = 0;
	//	this->y = 0;
	//	this->z = 0;
	//	this->w = 0;
	//}

	Vec4::Vec4(const float& x, const float& y, const float& z, const float& w) {
		this->x = x;
		this->y = y;
		this->z = z;
		this->w = w;
	}

	Vec4 operator+(const Vec4& left, const Vec4& right) {
		Vec4 r = left;
		r.add(right);
		return r;
	}

	Vec4 operator*(const Vec4& left, const Vec4& right) {
		Vec4 r = left;
		r.multiply(right);
		return r;
	}

	Vec4 operator-(const Vec4& left, const Vec4& right) {
		Vec4 r = left;
		r.subtract(right);
		return r;
	}

	Vec4 operator/(const Vec4& left, const Vec4& right) {
		Vec4 r = left;
		r.divide(right);
		return r;
	}

	Vec4& Vec4::add(Vec4 other) {
		x += other.x;
		y += other.y;
		z += other.z;
		w += other.w;
		return *this;
	}
	Vec4& Vec4::subtract(Vec4 other) {
		x -= other.x;
		y -= other.y;
		z -= other.z;
		w -= other.w;
		return *this;
	}
	Vec4& Vec4::multiply(Vec4 other) {
		x *= other.x;
		y *= other.y;
		z *= other.z;
		w *= other.w;
		return *this;
	}
	Vec4& Vec4::divide(Vec4 other) {
		x /= other.x;
		y /= other.y;
		z /= other.z;
		w /= other.w;
		return *this;
	}

	std::ostream& operator<<(std::ostream& stream, const Vec4& vector) {
		stream << std::to_string(vector.x) << " : " << std::to_string(vector.y) << " : " << std::to_string(vector.z) << " : " << std::to_string(vector.w);
		return stream;
	}



}}