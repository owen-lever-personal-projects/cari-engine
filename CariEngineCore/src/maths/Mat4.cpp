#include "Mat4.h"
#include "maths.h"

namespace CariEngine { namespace Maths {

	Mat4::Mat4(){
		for (int i = 0; i < 4 * 4; i++) {
			elements[i/4][i%4] = 0.0f;
		}

	}

	Mat4::Mat4(float diagonal) {
		for (int i = 0; i < 4 * 4; i++) {
			elements[i/4][i%4] = 0.0f;
		}
		for (int i = 0; i < 4; i++) {
			elements[i][i] = diagonal;
		}
	}

	Mat4 Mat4::identity() {
		return Mat4(1.0f);
	}

	Mat4 Mat4::ortho(float top, float bottom, float left, float right, float near, float far) {
		Mat4 result;

		result.elements[0][0] = 2.0f / (right - left);
		result.elements[1][1] = 2.0f / (top - bottom);
		result.elements[2][2] = 2.0f / (near - far);


		result.elements[3][0] = -(right + left) / (right - left);
		result.elements[3][1] = -(top + bottom) / (top - bottom);
		result.elements[3][2] = (near + far) / (near - far);


		result.elements[3][3] = 1.0f;
		return result;

	}

	Mat4 Mat4::persp(float fov, float aspectRatio, float near, float far) {
		Mat4 result;

		float q = 1.0f / tan(toRadians(0.5f * fov));
		float a = q / aspectRatio;
		float b = (near + far) / (near - far);
		float c = (2.0f * near * far) / (near - far);

		result.elements[0][0] = a;
		result.elements[1][1] = q;
		result.elements[2][2] = b;
		result.elements[3][2] = -1.0f;
		result.elements[2][3] = c;

		return result;
	}

	Mat4 Mat4::translation(const Vec3& translation) {
		Mat4 result = Mat4(1.0f);

		result.elements[3][0] = translation.x;
		result.elements[3][1] = translation.y;
		result.elements[3][2] = translation.z;

		return result;
	}

	Mat4 Mat4::scale(const Vec3& scale) {
		Mat4 result = Mat4();

		result.elements[0][0] = scale.x;
		result.elements[1][1] = scale.y;
		result.elements[2][2] = scale.z;
		result.elements[3][3] = 1.0f;

		return result;
	}

	Mat4 Mat4::inverse(const Mat4& matrix) {
		Mat4 inverse;

		const float (*m)[4] = matrix.elements;
		float (*i)[4] = inverse.elements;
		
		inverse.elements[0][0] = m[1][1] * m[2][2] * m[3][3] + m[2][1] * m[3][2] * m[1][3] + m[3][1] * m[1][2] * m[2][3]
								-m[1][1] * m[3][2] * m[2][3] - m[2][1] * m[1][2] * m[3][3] - m[3][1] * m[2][2] * m[1][3];

		inverse.elements[1][0] = m[1][0] * m[3][2] * m[2][3] + m[2][0] * m[1][2] * m[3][3] + m[3][0] * m[2][2] * m[1][3]
								-m[1][0] * m[2][2] * m[3][3] - m[2][0] * m[3][2] * m[1][3] - m[3][0] * m[1][2] * m[2][3];
		
		inverse.elements[2][0] = m[1][0] * m[2][1] * m[3][3] + m[2][0] * m[3][1] * m[1][3] + m[3][0] * m[1][1] * m[2][3]
								-m[1][0] * m[3][1] * m[2][3] - m[2][0] * m[1][1] * m[3][3] - m[3][0] * m[1][2] * m[2][3];

		inverse.elements[3][0] = m[1][0] * m[3][1] * m[2][2] + m[2][0] * m[1][1] * m[3][2] + m[3][0] * m[2][1] * m[3][1]
								-m[1][0] * m[2][1] * m[3][2] - m[2][0] * m[3][1] * m[1][2] - m[3][0] * m[1][1] * m[2][2];

		
		inverse.elements[0][1] = m[0][1] * m[3][2] * m[2][3] + m[2][1] * m[0][2] * m[3][3] + m[3][1] * m[2][2] * m[0][3]
								-m[0][1] * m[2][2] * m[3][3] - m[2][1] * m[3][2] * m[0][3] - m[3][1] * m[0][2] * m[2][3];

		inverse.elements[1][1] = m[0][0] * m[2][2] * m[3][3] + m[2][0] * m[3][2] * m[0][3] + m[3][0] * m[2][2] * m[2][3]
								-m[0][0] * m[3][2] * m[2][3] - m[2][0] * m[0][2] * m[3][3] - m[3][0] * m[2][2] * m[0][3];
		
		inverse.elements[2][1] = m[0][0] * m[3][1] * m[2][3] + m[2][0] * m[0][1] * m[3][3] + m[3][0] * m[2][1] * m[0][3]
								-m[0][0] * m[2][1] * m[3][3] - m[2][0] * m[3][1] * m[0][3] - m[3][0] * m[0][1] * m[2][3];

		inverse.elements[3][1] = m[0][0] * m[2][1] * m[3][2] + m[2][0] * m[3][1] * m[0][2] + m[3][0] * m[0][1] * m[2][2]
								-m[0][0] * m[3][1] * m[2][2] - m[2][0] * m[0][1] * m[3][2] - m[3][0] * m[2][1] * m[0][2];


		inverse.elements[0][2] = m[0][1] * m[1][2] * m[3][3] + m[1][1] * m[3][2] * m[0][3] + m[3][1] * m[0][2] * m[1][3]
								-m[0][1] * m[0][2] * m[3][1] - m[1][1] * m[0][2] * m[3][3] - m[3][1] * m[1][2] * m[0][3];
		
		inverse.elements[1][2] = m[0][0] * m[3][2] * m[1][3] + m[1][0] * m[0][2] * m[3][3] + m[3][0] * m[1][2] * m[0][3]
								-m[0][0] * m[1][2] * m[3][3] - m[1][0] * m[3][2] * m[0][3] - m[3][0] * m[0][2] * m[1][3];
		
		inverse.elements[2][2] = m[0][0] * m[1][1] * m[3][3] + m[1][0] * m[3][1] * m[0][3] + m[3][0] * m[0][1] * m[1][3]
								-m[0][0] * m[3][1] * m[1][3] - m[1][0] * m[0][1] * m[3][3] - m[3][0] * m[1][1] * m[0][3];

		inverse.elements[3][2] = m[0][0] * m[3][1] * m[1][2] + m[1][0] * m[0][1] * m[3][2] + m[3][0] * m[1][1] * m[0][2]
								-m[0][0] * m[1][1] * m[3][2] - m[1][0] * m[3][1] * m[0][2] - m[3][0] * m[0][1] * m[1][2];

		inverse.elements[0][3] = m[0][1] * m[2][2] * m[1][3] + m[1][1] * m[0][2] * m[2][3] + m[2][1] * m[1][2] * m[0][3]
								-m[0][1] * m[1][2] * m[2][3] - m[1][1] * m[2][2] * m[0][3] - m[2][1] * m[0][2] * m[1][3];

		inverse.elements[1][3] = m[0][0] * m[1][2] * m[2][3] + m[1][0] * m[2][2] * m[0][3] + m[2][0] * m[0][2] * m[1][3]
								-m[0][0] * m[2][2] * m[1][3] - m[1][0] * m[0][2] * m[2][3] - m[2][0] * m[1][2] * m[0][3];
		
		inverse.elements[2][3] = m[0][0] * m[2][1] * m[1][3] + m[1][0] * m[0][1] * m[2][3] + m[2][0] * m[1][1] * m[3][0]
								-m[0][0] * m[1][1] * m[2][3] - m[1][0] * m[2][1] * m[0][3] - m[2][0] * m[0][1] * m[1][3];

		inverse.elements[3][3] = m[0][0] * m[1][1] * m[2][2] + m[1][0] * m[2][1] * m[0][2] + m[2][0] * m[0][1] * m[1][2]
								-m[0][0] * m[2][1] * m[1][2] - m[1][0] * m[0][1] * m[2][2] - m[2][0] * m[1][1] * m[0][2];

		float det = m[0][0] * i[0][0] + m[0][1] * i[1][0] + m[0][2] * i[2][0] + m[0][3] * i[3][0];

		det = (float)1 / det;
		inverse *= det;

		return inverse;
	}

	Mat4& Mat4::operator/=(const float& right) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				elements[i][j] /= right;
			}
		}

		return *this;
	}

	Mat4& Mat4::operator*=(const float& right) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				elements[i][j] *= right;
			}
		}
		
		return *this;
	}
	
	Mat4 Mat4::rotation(float roll, float pitch, float yaw) {
		Mat4 result;
		
		float sr = sin(roll);
		float cr = cos(roll);

		float sp = sin(pitch);
		float cp = cos(pitch);

		float sy = sin(yaw);
		float cy = cos(yaw);



		result.elements[0][0] = cy*cr;
		result.elements[0][1] = sp*sy*cr + cp*sr;
		result.elements[0][2] = -cp*sy*cr+sp*sr;

		result.elements[1][0] = -cy*sr;
		result.elements[1][1] = -sp*sy*sr+cp*cr;
		result.elements[1][2] = cp*sy*sr+sp*cr;

		result.elements[2][0] = sy;
		result.elements[2][1] = -sp*cy;
		result.elements[2][2] = cp*cy;

		result.elements[3][3] = 1.0f;

		return result;
	}

	Mat4 Mat4::rotation(float angle, const Vec3& axis) {
		
		Mat4 result;
		Vec3 norm = axis;
		norm.normalise();

		float x = norm.x;
		float y = norm.y;
		float z = norm.z;

		float a = toRadians(angle);
		float s = sin(a);
		float c = cos(a);
		float mc = 1.0f - c;

		result.elements[0][0] = (x * x * mc) +		c;
		result.elements[1][0] = (y * x * mc) + z * s;
		result.elements[2][0] = (z * x * mc) - y * s;
		
		result.elements[0][1] = (x * y * mc) - z * s;
		result.elements[1][1] = (y * y * mc) +		c;
		result.elements[2][1] = (z * y * mc) + x * s;
		
		result.elements[0][2] = (x * z * mc) + y * s;
		result.elements[1][2] = (y * z * mc) - x * s;
		result.elements[2][2] = (z * z * mc) +		c;



		result.elements[3][3] = 1.0f;

		return result;
	}

	Mat4 Mat4::lookAt(const Vec3& pos, const Vec3& target, const Vec3& up) {
		Vec3 fwd = (pos - target);
		fwd.normalise();

		Vec3 u = up;
		u.normalise();
		
		Vec3 l = Vec3::cross(up, fwd);
		l.normalise();

		u = Vec3::cross(fwd, l).normalise();


		Mat4 result;
		result.elements[0][0] = l.x;
		result.elements[0][1] = u.x;
		result.elements[0][2] = fwd.x;

		result.elements[1][0] = l.y;
		result.elements[1][1] = u.y;
		result.elements[1][2] = fwd.y;

		result.elements[2][0] = l.z;
		result.elements[2][1] = u.z;
		result.elements[2][2] = fwd.z;

		result.elements[3][0] = -(Vec3::dot(l, pos));
		result.elements[3][1] = -(Vec3::dot(u, pos));
		result.elements[3][2] = -(Vec3::dot(fwd, pos));

		result.elements[3][3] = 1;

		return result;
	}

	Mat4& Mat4::multiply(const Mat4& other) {
		float newElements[4][4];
		for (int y = 0; y < 4; y++) {
			for (int x = 0; x < 4; x++) {
				float sum = 0.0f;
				for (int e = 0; e < 4; e++) {
					sum += elements[x][e] * other.elements[e][y];
				}
				newElements[x][y] = sum;

			}
		}
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {

				elements[i][j] = newElements[i][j];
			}
		}
		return *this;
	}

	/*
	Vec4 operator*(const Mat4& left, const Vec4& right) {
		Vec4 result;

		result.x += left.elements[0][0] * right.x;
		result.x += left.elements[0][1] * right.y;
		result.x += left.elements[0][2] * right.z;
		result.x += left.elements[0][3] * right.w;

		result.y += left.elements[1][0] * right.x;
		result.y += left.elements[1][1] * right.y;
		result.y += left.elements[1][2] * right.z;
		result.y += left.elements[1][3] * right.w;

		result.z += left.elements[2][0] * right.x;
		result.z += left.elements[2][1] * right.y;
		result.z += left.elements[2][2] * right.z;
		result.z += left.elements[2][3] * right.w;

		result.w += left.elements[3][0] * right.x;
		result.w += left.elements[3][1] * right.y;
		result.w += left.elements[3][2] * right.z;
		result.w += left.elements[3][3] * right.w;

		return result;
	}

	Vec3 operator*(const Mat4& left, const Vec3& right) {
		Vec3 result;

		result.x += left.elements[0 + 0 * 4] * right.x;
		result.x += left.elements[0 + 1 * 4] * right.y;
		result.x += left.elements[0 + 2 * 4] * right.z;
		result.x += left.elements[0 + 3 * 4];

		result.y += left.elements[1 + 3 * 4] * right.x;
		result.y += left.elements[1 + 3 * 4] * right.y;
		result.y += left.elements[1 + 3 * 4] * right.z;
		result.y += left.elements[1 + 3 * 4];

		result.z += left.elements[2 + 3 * 4] * right.x;
		result.z += left.elements[2 + 3 * 4] * right.y;
		result.z += left.elements[2 + 3 * 4] * right.z;
		result.z += left.elements[2 + 3 * 4];

		return result;
	}
	*/

	Mat4 operator*(const Mat4& left, const Mat4& right) {
		Mat4 result = left;
		return result.multiply(right);

	}
	Mat4 Mat4::operator*=(const Mat4& other) {
		return multiply(other);
	}

	std::string Mat4::toString() const{
		std::string result;

		for (int i = 0; i, i < 4; i++) {
			result += "[ " + std::to_string(elements[0][i]) + " : " + std::to_string(elements[1][i]) + " : " + std::to_string(elements[2][i]) + " : " + std::to_string(elements[3][i]) + "]";
			if (i != 4) result += "\n";
		}


		return result;
	}

	std::ostream& operator<<(std::ostream& stream, const Mat4& matrix) {
		stream << matrix.toString();
		return stream;
	}


}}