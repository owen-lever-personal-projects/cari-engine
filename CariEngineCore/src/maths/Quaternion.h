#pragma once
#include <iostream>
#include "maths.h"
namespace CariEngine { namespace Maths { 
	
	
	struct Quaternion {
		union { float s; float r; };
		union { float x; float i; };
		union { float y; float j; };
		union { float z; float k; };
		
		Quaternion() = default;
		Quaternion(const float& s, const float& x, const float& y, const float& z);
		Quaternion(const Vec4& v);
		Quaternion(const float& s, const Vec3& v);

		Quaternion& add(Quaternion other);
		Quaternion& subtract(Quaternion other);
		Quaternion& multiply(Quaternion other);
		Quaternion& divide(Quaternion other);

		Vec3 getVec3() const;
		

		Quaternion& normalise();
		float length() const;

		static Quaternion cross(const Quaternion& a, const Quaternion& b);
		static float dot(const Quaternion& a, const Quaternion& b);
		static Quaternion normalised(const Quaternion& a);

		friend std::ostream& operator<<(std::ostream& stream, const Quaternion& vector);

		Quaternion& operator+=(const Quaternion& right);
		Quaternion& operator*=(const Quaternion& right);
		Quaternion& operator-=(const Quaternion& right);
		Quaternion& operator/=(const Quaternion& right);

		friend Quaternion operator+(const Quaternion& left, const Quaternion& right);
		friend Quaternion operator*(const Quaternion& left, const Quaternion& right);
		friend Quaternion operator-(const Quaternion& left, const Quaternion& right);
		friend Quaternion operator/(const Quaternion& left, const Quaternion& right);

		friend Quaternion operator*(const Quaternion& left, const float& right);
		friend Quaternion operator/(const Quaternion& left, const float& right);

		friend bool operator<(const Quaternion& left, const Quaternion& right);
		friend bool operator==(const Quaternion& left, const Quaternion& right);










	};
}}