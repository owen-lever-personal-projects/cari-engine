#pragma once
#include <math.h>
#include <iostream>
#include <string>


namespace CariEngine { namespace Maths {
	struct Vec3 
	{
		float x;
		float y;
		float z;

		Vec3();
		Vec3(const float& x, const float& y, const float& z);
		Vec3(const float& v);

		Vec3& add(const Vec3& other);
		Vec3& subtract(const Vec3& other);
		Vec3& multiply(const Vec3& other);
		Vec3& divide(const Vec3& other);

		Vec3& multiply(const float& other);
		Vec3& divide(const float& other);

		Vec3& normalise();
		float length() const;

		static Vec3 cross(const Vec3& a, const Vec3& b);
		static float dot(const Vec3& a, const Vec3& b);
		static Vec3 max(const Vec3& a, const Vec3& b);
		static Vec3 normalised(const Vec3& a);
		static float distance(const Vec3& a, const Vec3& b);
		static Vec3 rotate(const Vec3& vector, const Vec3& axis, const float& angle);
		static Vec3 getDirectionDegrees(float hRotation, float vRotation);
		static Vec3 getDirection(float hRotation, float vRotation);


		friend std::ostream& operator<<(std::ostream& stream, const Vec3& vector);
		std::string toString() const;
		
		friend Vec3 operator+(const Vec3& left, const Vec3& right);
		friend Vec3 operator*(const Vec3& left, const Vec3& right);
		friend Vec3 operator-(const Vec3& left, const Vec3& right);
		friend Vec3 operator/(const Vec3& left, const Vec3& right);

		friend Vec3 operator*(const Vec3& left, const float& right);
		friend Vec3 operator/(const Vec3& left, const float& right);

		Vec3& operator+=(const Vec3& right);
		Vec3& operator*=(const Vec3& right);
		Vec3& operator-=(const Vec3& right);
		Vec3& operator/=(const Vec3& right);

		Vec3& operator*=(const float& right);
		Vec3& operator/=(const float& right);

		friend bool operator<(const Vec3& left, const Vec3& right);
		friend bool operator>(const Vec3& left, const Vec3& right);
		friend bool operator<=(const Vec3& left, const Vec3& right);
		friend bool operator>=(const Vec3& left, const Vec3& right);
		friend bool operator==(const Vec3& left, const Vec3& right);
	};




}}