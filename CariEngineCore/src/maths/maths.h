#pragma once

#define _USE_MATH_DEFINES
#include "Vec2.h"
#include "Vec3.h"
#include "Vec4.h"
#include "Mat4.h"

static const float pi = 3.14159265358979f;

static float toRadians(float degrees)
{
	return degrees * (pi / 180.0f);
}

static float toDegrees(float radians)
{
	return radians * (180.0f / pi);
}

static float maxf(float a, float b) {
	if (a > b)return a;
	return b;
}

static int exp(int base, int e) {
	if (e == 0) return 1;
	if (e < 0) return 0;

	if (e % 2 == 0) {
		int r = exp(base, e>>1);
		return r * r;
	}

	return base * exp(base, e - 1);
}

static float exp(float base, int e) {
	if (e == 0) return 1;
	if (e < 0) return 1 / exp(base, -e);
	
	if (e % 2 == 0) {
		int r = exp(base, e >> 1);
		return r * r;
	}

	return base * exp(base, e - 1);
}


// https://gist.github.com/badboy/6267743

static unsigned int mix(int a, int b, int c) {
	//Robert Jenkins' 96 bit Mix Function
	a -= b; a -= c; a ^= (c >> 13);
	b -= c; b -= a; b ^= (a << 8);
	c -= a; c -= b; c ^= (b >> 13);
	a -= b; a -= c; a ^= (c >> 12);
	b -= c; b -= a; b ^= (a << 16);
	c -= a; c -= b; c ^= (b >> 5);
	a -= b; a -= c; a ^= (c >> 3);
	b -= c; b -= a; b ^= (a << 10);
	c -= a; c -= b; c ^= (b >> 15);
	return c;
}

static unsigned int mix(unsigned long key) {
	key = (~key) + (key << 18); // key = (key << 18) - key - 1;
	key = key ^ (key >> 31);
	key = key * 21; // key = (key + (key << 2)) + (key << 4);
	key = key ^ (key >> 11);
	key = key + (key << 6);
	key = key ^ (key >> 22);
	return (int)key;
}

static unsigned int mix(unsigned int a, unsigned int b) {
	return mix((long)a << 32 | b);
}


static unsigned int hash(unsigned int a) {
	//Robert Jenkins' 32 bit integer hash function
	a = (a + 0x7ed55d16) + (a << 12);
	a = (a ^ 0xc761c23c) ^ (a >> 19);
	a = (a + 0x165667b1) + (a << 5);
	a = (a + 0xd3a2646c) ^ (a << 9);
	a = (a + 0xfd7046c5) + (a << 3);
	a = (a ^ 0xb55a4f09) ^ (a >> 16);
	return a;
}