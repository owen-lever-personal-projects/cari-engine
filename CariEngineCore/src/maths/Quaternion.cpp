#include "Quaternion.h"

namespace CariEngine { namespace Maths {
	
	Quaternion::Quaternion(const float& s, const float& x, const float& y, const float& z) {
		this->s = s;
		this->x = x;
		this->y = y;
		this->z = z;
	}

	Quaternion operator+(const Quaternion& left, const Quaternion& right) {
		Quaternion r = left;
		r.add(right);
		return r;
	}

	Quaternion operator*(const Quaternion& left, const Quaternion& right) {
		Quaternion r = left;
		r.multiply(right);
		return r;
	}

	Quaternion operator-(const Quaternion& left, const Quaternion& right) {
		Quaternion r = left;
		r.subtract(right);
		return r;
	}

	Quaternion operator/(const Quaternion& left, const Quaternion& right) {
		Quaternion r = left;
		r.divide(right);
		return r;
	}

	Quaternion& Quaternion::add(Quaternion other) {
		s += other.s;
		x += other.x;
		y += other.y;
		z += other.z;
		return *this;
	}
	Quaternion& Quaternion::subtract(Quaternion other) {
		s -= other.s;
		x -= other.x;
		y -= other.y;
		z -= other.z;
		return *this;
	}

	Quaternion& Quaternion::multiply(Quaternion other) {

		float tmpR(0);
		tmpR += s * other.s;
		tmpR -= x * other.x;
		tmpR -= y * other.y;
		tmpR -= z * other.z;

		float tmpI(0);
		tmpI += s * other.x;
		tmpI += x * other.s;
		tmpI += y * other.z;
		tmpI -= z * other.y;

		float tmpJ(0);
		tmpJ += s * other.y;
		tmpJ -= x * other.z;
		tmpJ += y * other.s;
		tmpJ += z * other.x;

		float tmpK(0);
		tmpK += s * other.z;
		tmpK += x * other.y;
		tmpK -= y * other.x;
		tmpK += z * other.s;

		r = tmpR;
		i = tmpI;
		j = tmpJ;
		k = tmpK;

		return *this;
	}

	Quaternion& Quaternion::divide(Quaternion other) {
		return *this;
	}

	Vec3 Quaternion::getVec3() const{
		return Vec3(x, y, z);
	}



}}