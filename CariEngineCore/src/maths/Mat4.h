#pragma once

#define _USE_MATH_DEFINES
#include "maths.h"
#include <string>

namespace CariEngine { namespace Maths {

	struct Mat4 {
		union {
			float elements[4][4];
			Vec4 columns[4];
		};
		
		Mat4();
		Mat4(float diagonal);

		static Mat4 identity();

		static Mat4 ortho(float top, float bottom, float left, float right, float near, float far);
		static Mat4 persp(float fov, float aspectRatio, float near, float far);

		static Mat4 translation(const Vec3& translation);
		static Mat4 rotation(float angle, const Vec3& axis);
		static Mat4 rotation(float roll, float pitch, float yaw);
		static Mat4 scale(const Vec3& scale);
		static Mat4 inverse(const Mat4& matrix);

		static Mat4 lookAt(const Vec3& pos, const Vec3& target, const Vec3& up);
		
		Mat4& multiply(const Mat4& other);

		friend Mat4 operator*(const Mat4& left, const Mat4& right);
		//friend Vec4 operator*(const Mat4& left, const Vec4& right);
		//friend Vec3 operator*(const Mat4& left, const Vec3& right);

		Mat4& operator*=(const float& right);
		Mat4& operator/=(const float& right);
		
		Mat4 operator*=(const Mat4& other);


		std::string toString() const;
		friend std::ostream& operator<<(std::ostream& stream, const Mat4& matrix);


	};


}}