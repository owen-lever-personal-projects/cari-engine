#include "Vec2.h"
#include <string>

namespace CariEngine { namespace Maths {
	
	Vec2::Vec2() {
		this->x = 0;
		this->y = 0;
	}

	Vec2::Vec2(const float& x, const float& y) {
		this->x = x;
		this->y = y;
	}

	Vec2::Vec2(const float& v) {
		this->x = v;
		this->y = v;
	}

	Vec2 operator+(const Vec2& left, const Vec2& right) {
		Vec2 r = left;
		r.add(right);
		return r;
	}

	Vec2 operator*(const Vec2& left, const Vec2& right) {
		Vec2 r = left;
		r.multiply(right);
		return r;
	}

	Vec2 operator-(const Vec2& left, const Vec2& right) {
		Vec2 r = left;
		r.subtract(right);
		return r;
	}

	Vec2 operator/(const Vec2& left, const Vec2& right) {
		Vec2 r = left;
		r.divide(right);
		return r;
	}

	Vec2& Vec2::add(Vec2 other) {
		x += other.x;
		y += other.y;
		return *this;
	}
	Vec2& Vec2::subtract(Vec2 other) {
		x -= other.x;
		y -= other.y;
		return *this;
	}
	Vec2& Vec2::multiply(Vec2 other) {
		x *= other.x;
		y *= other.y;
		return *this;
	}
	Vec2& Vec2::divide(Vec2 other) {
		x /= other.x;
		y /= other.y;
		return *this;
	}

	std::ostream& operator<<(std::ostream& stream, const Vec2& vector) {
		stream << std::to_string(vector.x) << " : " << std::to_string(vector.y);
		return stream;
	}

	Vec2& Vec2::operator+=(const Vec2& right) {
		return add(right);
	}

	Vec2& Vec2::operator*=(const Vec2& right) {
		return multiply(right);
	}

	Vec2& Vec2::operator-=(const Vec2& right) {
		return subtract(right);
	}

	Vec2& Vec2::operator/=(const Vec2& right) {
		return divide(right);
	}

	float Vec2::length() const {
		return sqrt(x * x + y * y);
	}

	Vec2& Vec2::normalise() {
		float length = this->length();
		x /= length;
		y /= length;
		return *this;
	}

	float Vec2::dot(const Vec2& a, const Vec2& b) {

		return (a.x * b.x) + (a.y * b.y);
	}

	Vec2 operator*(const Vec2& left, const float& right) {
		Vec2 r = left;
		r.x *= right;
		r.y *= right;
		return r;
	}

	Vec2 operator/(const Vec2& left, const float& right) {
		Vec2 r = left;
		r.x /= right;
		r.y /= right;
		return r;
	}

}}