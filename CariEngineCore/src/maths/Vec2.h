#pragma once
#include <iostream>

namespace CariEngine { namespace Maths {
	struct Vec2 
	{
		float x;
		float y;

		Vec2();
		Vec2(const float& x, const float& y);
		Vec2(const float& v);

		Vec2& add(Vec2 other);
		Vec2& subtract(Vec2 other);
		Vec2& multiply(Vec2 other);
		Vec2& divide(Vec2 other);
		
		Vec2& normalise();
		float length() const;

		static Vec2 cross(const Vec2& a, const Vec2& b);
		static float dot(const Vec2& a, const Vec2& b);
		static Vec2 normalised(const Vec2& a);

		friend std::ostream& operator<<(std::ostream& stream, const Vec2& vector);

		Vec2& operator+=(const Vec2& right);
		Vec2& operator*=(const Vec2& right);
		Vec2& operator-=(const Vec2& right);
		Vec2& operator/=(const Vec2& right);

		friend Vec2 operator+(const Vec2& left, const Vec2& right);
		friend Vec2 operator*(const Vec2& left, const Vec2& right);
		friend Vec2 operator-(const Vec2& left, const Vec2& right);
		friend Vec2 operator/(const Vec2& left, const Vec2& right);

		friend Vec2 operator*(const Vec2& left, const float& right);
		friend Vec2 operator/(const Vec2& left, const float& right);

		friend bool operator<(const Vec2& left, const Vec2& right);
		friend bool operator==(const Vec2& left, const Vec2& right);
	};




}}


/*
struct Vec3
	{
		float x;
		float y;
		float z;

		Vec3();
		Vec3(const float& x, const float& y, const float& z);

		Vec3& add(const Vec3& other);
		Vec3& subtract(const Vec3& other);
		Vec3& multiply(const Vec3& other);
		Vec3& divide(const Vec3& other);

		Vec3& normalise();
		float length() const;

		static Vec3 cross(const Vec3& a, const Vec3& b);
		static float dot(const Vec3& a, const Vec3& b);
		static Vec3 max(const Vec3& a, const Vec3& b);
		static Vec3 normalised(const Vec3& a);


		friend std::ostream& operator<<(std::ostream& stream, const Vec3& vector);

		Vec3& operator+=(const Vec3& right);
		Vec3& operator*=(const Vec3& right);
		Vec3& operator-=(const Vec3& right);
		Vec3& operator/=(const Vec3& right);

		friend Vec3 operator+(const Vec3& left, const Vec3& right);
		friend Vec3 operator*(const Vec3& left, const Vec3& right);
		friend Vec3 operator-(const Vec3& left, const Vec3& right);
		friend Vec3 operator/(const Vec3& left, const Vec3& right);

		friend Vec3 operator*(const Vec3& left, const float& right);
		friend Vec3 operator/(const Vec3& left, const float& right);

		friend bool operator<(const Vec3& left, const Vec3& right);
		friend bool operator==(const Vec3& left, const Vec3& right);
	};
*/