
#include "src/core/Application.h"
#include "src/test/TestGame.h"

using namespace CariEngine;
using namespace Graphics;
using namespace Maths;


int __stdcall WinMain(void*, void*, char* cmdLine, int)
{

	TestGame game;
	Application app = Application(&game);

	return 0;
}


int main() {
	TestGame game;
	Application app = Application(&game);
	app.init();
	app.run();
	return 0;
}
